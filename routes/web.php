<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('auth.login'); });
Route::get('/unauth', function () { return view('unauth'); });

Route::middleware(['auth','ipcheck'])->group(function () {
    
    // Home Page Routes - Charts Controller
    Route::GET('/home', 'chartController@chart')->name('home')->middleware(['Report']);
    
    // Home Routes - agents Details
    Route::GET('/home/agentsDetails', 'chartController@agentsDetails')->name('agentsDetails')->middleware(['Report']);
    Route::POST('/home/agentsDetails/search', 'chartController@agentSearchDetails')->name('agentSearchDetails')->middleware(['Report']);
    
    // Home Routes - messagesDetails
    Route::GET('/home/messagesDetails', 'chartController@messagesDetails')->name('messagesDetails')->middleware(['Report']);
    Route::POST('/home/messagesDetails/search', 'chartController@messagesSearchDetails')->name('messagesSearchDetails')->middleware(['Report']);
    
    // Home Routes - transactionsDetails
    Route::GET('/home/transactionsDetails', 'chartController@transactionsDetails')->name('transactionsDetails')->middleware(['Report']);
    Route::POST('/home/transactionsDetails/search', 'chartController@transactionsSearchDetails')->name('transactionsSearchDetails')->middleware(['Report']);

    // Home Routes - paymentsDetails
    Route::GET('/home/paymentsDetails', 'chartController@paymentsDetails')->name('paymentsDetails')->middleware(['Report']);
    Route::POST('/home/paymentsDetails/search', 'chartController@paymentsSearchDetails')->name('paymentsSearchDetails')->middleware(['Report']);

// Details
Route::GET('/details', 'ReportController@indexDetails')->name('indexDetails')->middleware(['Report']);
Route::POST('/details/search', 'ReportController@searchDetails')->name('searchDetails')->middleware(['Report']);

// Admins Routes
Route::GET('/admins', 'AdminController@index')->name('adminIndex')->middleware(['admins']);
Route::GET('/admins/create', 'AdminController@create')->name('adminCreate')->middleware(['admins']);
Route::POST('/admins', 'AdminController@store')->name('adminStore')->middleware(['admins']);
Route::GET('/admins/{admin}', 'AdminController@show')->name('adminShow')->middleware(['admins']);
Route::GET('/admins/{admin}/edit', 'AdminController@edit')->name('adminEdit')->middleware(['admins']);
Route::PUT('/admins/{admin}', 'AdminController@update')->name('adminUpdate')->middleware(['admins']);
Route::GET('/admins/{admin}/destroy', 'AdminController@destroy')->name('adminDestroy')->middleware(['admins']);
Route::GET('/admins/{admin}/activation', 'AdminController@activation')->name('adminActivation')->middleware(['admins']);

// Agents Routes
Route::GET('/agents', 'AgentController@index')->name('agentIndex');
Route::GET('/agents/create', 'AgentController@create')->name('agentCreate')->middleware(['user']);
Route::POST('/agents', 'AgentController@store')->name('agentStore');
Route::GET('/agents/{agent}', 'AgentController@show')->name('agentShow')->middleware(['user']);
Route::GET('/agents/{agent}/edit', 'AgentController@edit')->name('agentEdit')->middleware(['user']);
Route::PUT('/agents/{agent}', 'AgentController@update')->name('agentUpdate')->middleware(['user']);
Route::GET('/agents/{agent}/destroy', 'AgentController@destroy')->name('agentDestroy')->middleware(['user']);
Route::GET('/agents/{agent}/activation', 'AgentController@activation')->name('agentActivation')->middleware(['user']);

// AgentMessages Routes
Route::GET('/agentMessages', 'AgentMessageController@index')->name('agentMessageIndex');
Route::GET('/agentMessages/create', 'AgentMessageController@create')->name('agentMessageCreate')->middleware(['user']);
Route::POST('/agentMessages', 'AgentMessageController@store')->name('agentMessageStore')->middleware(['user']);
Route::GET('/agentMessages/{message}', 'AgentMessageController@show')->name('agentMessageShow')->middleware(['user']);
Route::GET('/agentMessages/{message}/edit', 'AgentMessageController@edit')->name('agentMessageEdit')->middleware(['user']);
Route::PUT('/agentMessages/{message}', 'AgentMessageController@update')->name('agentMessageUpdate')->middleware(['user']);
Route::GET('/agentMessages/{message}/destroy', 'AgentMessageController@destroy')->name('agentMessageDestroy')->middleware(['user']);



// roles Routes
Route::GET('/roles', 'RoleController@index')->name('roleIndex')->middleware(['admins']);
Route::GET('/roles/create', 'RoleController@create')->name('roleCreate')->middleware(['admins']);
Route::POST('/roles', 'RoleController@store')->name('roleStore')->middleware(['admins']);
Route::GET('/roles/{role}', 'RoleController@show')->name('roleShow')->middleware(['admins']);
Route::GET('/roles/{role}/edit', 'RoleController@edit')->name('roleEdit')->middleware(['admins']);
Route::PUT('/roles/{role}', 'RoleController@update')->name('roleUpdate')->middleware(['admins']);
Route::GET('/roles/{role}/destroy', 'RoleController@destroy')->name('roleDestroy')->middleware(['admins']);

// login History Routes
Route::GET('/logins', 'LoginHistoryController@index')->name('loginIndex')->middleware(['admins']);
Route::GET('/logins/create', 'LoginHistoryController@create')->name('loginCreate')->middleware(['admins']);
Route::POST('/logins', 'LoginHistoryController@store')->name('loginStore')->middleware(['admins']);
Route::GET('/logins/{login}', 'LoginHistoryController@show')->name('loginShow')->middleware(['admins']);
Route::GET('/logins/{login}/edit', 'LoginHistoryController@edit')->name('loginEdit')->middleware(['admins']);
Route::PUT('/logins/{login}', 'LoginHistoryController@update')->name('loginUpdate')->middleware(['admins']);
Route::GET('/logins/{login}/destroy', 'LoginHistoryController@destroy')->name('loginDestroy')->middleware(['admins']);
Route::GET('/logins/{login}/activation', 'LoginHistoryController@activation')->name('loginActivation')->middleware(['admins']);

// Agent Companies Routes
Route::GET('/agentCompanies', 'AgentCompanyController@index')->name('agentCompanyIndex')->middleware(['admins']);
Route::GET('/agentCompanies/create', 'AgentCompanyController@create')->name('agentCompanyCreate')->middleware(['admins']);
Route::POST('/agentCompanies', 'AgentCompanyController@store')->name('agentCompanyStore')->middleware(['admins']);
Route::GET('/agentCompanies/{companyAgent}', 'AgentCompanyController@show')->name('agentCompanyShow')->middleware(['admins']);
Route::GET('/agentCompanies/{companyAgent}/edit', 'AgentCompanyController@edit')->name('agentCompanyEdit')->middleware(['admins']);
Route::PUT('/agentCompanies/{companyAgent}', 'AgentCompanyController@update')->name('agentCompanyUpdate')->middleware(['admins']);
Route::GET('/agentCompanies/{companyAgent}/destroy', 'AgentCompanyController@destroy')->name('agentCompanyDestroy')->middleware(['admins']);

// Companies Routes
Route::GET('/companies', 'CompanyController@index')->name('companyIndex')->middleware(['admins']);
Route::GET('/companies/create', 'CompanyController@create')->name('companyCreate')->middleware(['admins']);
Route::POST('/companies', 'CompanyController@store')->name('companyStore')->middleware(['admins']);
Route::GET('/companies/{companyAgent}', 'CompanyController@show')->name('companyShow')->middleware(['admins']);
Route::GET('/companies/{companyAgent}/edit', 'CompanyController@edit')->name('companyEdit')->middleware(['admins']);
Route::PUT('/companies/{companyAgent}', 'CompanyController@update')->name('companyUpdate')->middleware(['admins']);
Route::GET('/companies/{companyAgent}/destroy', 'CompanyController@destroy')->name('companyDestroy')->middleware(['admins']);

// Agent Transactions Routes
Route::GET('/agentTransactions', 'AgentTransactionController@index')->name('agentTransactionIndex')->middleware(['user']);
Route::GET('/agentTransactions/create', 'AgentTransactionController@create')->name('agentTransactionCreate')->middleware(['user']);
Route::POST('/agentTransactions', 'AgentTransactionController@store')->name('agentTransactionStore')->middleware(['user']);
Route::GET('/agentTransactions/{agentTransaction}', 'AgentTransactionController@show')->name('agentTransactionShow')->middleware(['user']);
Route::GET('/agentTransactions/{agentTransaction}/edit', 'AgentTransactionController@edit')->name('agentTransactionEdit')->middleware(['user']);
Route::PUT('/agentTransactions/{agentTransaction}', 'AgentTransactionController@update')->name('agentTransactionUpdate')->middleware(['user']);
Route::GET('/agentTransactions/{agentTransaction}/destroy', 'AgentTransactionController@destroy')->name('agentTransactionDestroy')->middleware(['user']);

// transactions Routes
Route::GET('/transactions', 'TransactionController@index')->name('transactionIndex')->middleware(['user']);
Route::GET('/transactions/create', 'TransactionController@create')->name('transactionCreate')->middleware(['user']);
Route::POST('/transactions', 'TransactionController@store')->name('transactionStore')->middleware(['user']);
Route::GET('/transactions/{agentTransaction}/edit', 'TransactionController@edit')->name('transactionEdit')->middleware(['user']);
Route::PUT('/transactions/{agentTransaction}', 'TransactionController@update')->name('transactionUpdate')->middleware(['user']);
Route::GET('/transactions/{transaction}/destroy', 'TransactionController@destroy')->name('transactionDestroy')->middleware(['user']);

// messages Routes
Route::GET('/message', 'MessageController@index')->name('messageIndex')->middleware(['admins']);
Route::GET('/message/create', 'MessageController@create')->name('messageCreate')->middleware(['admins']);
Route::POST('/message', 'MessageController@store')->name('messageStore')->middleware(['admins']);
Route::GET('/message/{role}/edit', 'MessageController@edit')->name('messageEdit')->middleware(['admins']);
Route::PUT('/message/{role}', 'MessageController@update')->name('messageUpdate')->middleware(['admins']);
Route::GET('/message/{role}/destroy', 'MessageController@destroy')->name('messageDestroy')->middleware(['admins']);

// messagesTypes Routes
Route::GET('/messagType', 'MessageTypeController@index')->name('messageTypeIndex')->middleware(['admins']);
Route::GET('/messagType/create', 'MessageTypeController@create')->name('messageTypeCreate')->middleware(['admins']);
Route::POST('/messagType', 'MessageTypeController@store')->name('messageTypeStore')->middleware(['admins']);
Route::GET('/messagType/{role}/edit', 'MessageTypeController@edit')->name('messageTypeEdit')->middleware(['admins']);
Route::PUT('/messagType/{role}', 'MessageTypeController@update')->name('messageTypeUpdate')->middleware(['admins']);
Route::GET('/messagType/{role}/destroy', 'MessageTypeController@destroy')->name('messageTypeDestroy')->middleware(['admins']);

// nationalities Routes
Route::GET('/nationalities', 'NationalityController@index')->name('nationalityIndex')->middleware(['admins']);
Route::GET('/nationalities/create', 'NationalityController@create')->name('nationalityCreate')->middleware(['admins']);
Route::POST('/nationalities', 'NationalityController@store')->name('nationalityStore')->middleware(['admins']);
Route::GET('/nationalities/{role}/edit', 'NationalityController@edit')->name('nationalityEdit')->middleware(['admins']);
Route::PUT('/nationalities/{role}', 'NationalityController@update')->name('nationalityUpdate')->middleware(['admins']);
Route::GET('/nationalities/{role}/destroy', 'NationalityController@destroy')->name('nationalityDestroy')->middleware(['admins']);

// Blocked Users Routes
Route::GET('/blockedUsers', 'UserBlockingController@index')->name('blockedUsersIndex')->middleware(['admins']);
Route::GET('/blockedUsers/create', 'UserBlockingController@create')->name('blockedUsersCreate')->middleware(['admins']);
Route::POST('/blockedUsers', 'UserBlockingController@store')->name('blockedUsersStore')->middleware(['admins']);
Route::GET('/blockedUsers/{user}/edit', 'UserBlockingController@edit')->name('blockedUsersEdit')->middleware(['admins']);
Route::PUT('/blockedUsers/{user}', 'UserBlockingController@update')->name('blockedUsersUpdate')->middleware(['admins']);
Route::GET('/blockedUsers/{user}/destroy', 'UserBlockingController@destroy')->name('blockedUsersDestroy')->middleware(['admins']);


// nationalities Routes
Route::GET('/languages', 'LanguageController@index')->name('languageIndex')->middleware(['admins']);
Route::GET('/languages/create', 'LanguageController@create')->name('languageCreate')->middleware(['admins']);
Route::POST('/languages', 'LanguageController@store')->name('languageStore')->middleware(['admins']);
Route::GET('/languages/{role}/destroy', 'LanguageController@destroy')->name('languageDestroy')->middleware(['admins']);

// AppSetting Routes
Route::GET('/appSetting', 'AppSettingController@index')->name('appSettingIndex')->middleware(['admins']);
Route::GET('/appSetting/{user}/edit', 'AppSettingController@edit')->name('appSettingEdit')->middleware(['admins']);
Route::PUT('/appSetting/{user}', 'AppSettingController@update')->name('appSettingUpdate')->middleware(['admins']);

Route::POST('/appSetting/import', 'AppSettingController@import')->name('appSettingImport')->middleware(['admins']);
Route::GET('/appSetting/export', 'AppSettingController@export')->name('appSettingExport')->middleware(['admins']);
// Reports Routes
Route::GET('/report', 'ReportController@index')->name('reportIndex')->middleware(['Report'])->middleware(['admins']);
Route::POST('/report/search/', 'ReportController@index')->name('search')->middleware(['Report'])->middleware(['admins']);

// loginLog Routes
Route::GET('/IndexLog', 'AppSettingController@IndexLog')->name('IndexLog')->middleware(['admins']);


// LatePayments Routes
Route::GET('/latePayments', 'LatePaymentController@index')->name('LatePaymentIndex')->middleware(['Report']);
Route::GET('/latePayments/{agent}/getPayed', 'LatePaymentController@LatePaymentGetPayed')->name('agentGetPayed')->middleware(['admins']);
// Route::GET('/agentReport/{agent}', 'ReportController@agentReportShow')->name('agentReportShow')->middleware(['Report']);
// Route::POST('/agentReport/{agent}/search', 'ReportController@agentReportSearch')->name('agentReportSearch')->middleware(['Report']);
// Route::POST('/agentInfo/{agent}', 'ReportController@agentInfo')->name('agentInfo')->middleware(['Report']);
// Route::GET('/message/{role}/destroy', 'MessageController@destroy')->name('messageDestroy');


// Agent Reports Routes
Route::GET('/agentReport', 'ReportController@agentIndex')->name('agentReportIndex')->middleware(['Report']);
Route::GET('/agentReport/{agent}', 'ReportController@agentReportShow')->name('agentReportShow')->middleware(['Report']);
Route::POST('/agentReport/{agent}/search', 'ReportController@agentReportSearch')->name('agentReportSearch')->middleware(['Report']);
Route::POST('/agentInfo/{agent}', 'ReportController@agentInfo')->name('agentInfo')->middleware(['Report']);

Route::GET('/message/{role}/destroy', 'MessageController@destroy')->name('messageDestroy');

Route::GET('/agentPayments', 'AgentPaymentController@index')->name('indexAgentPayment')->middleware(['admins']);
Route::GET('/agentPayments/{agent}', 'AgentPaymentController@show')->name('showAgentPayment')->middleware(['admins']);
Route::GET('/agentPayments/{agent}/getPayed', 'AgentPaymentController@agentGetPayed')->name('agentGetPayed')->middleware(['admins']);

});
Auth::routes();

Route::GET('/agentPhone', 'AgentController@phonePage')->name('phonePage');
Route::POST('/agentPhone', 'AgentController@checkphone')->name('checkPhone');

Route::GET('/agentCode', 'AgentController@codePage')->name('codePage');
Route::POST('/agentCode', 'AgentController@checkCode')->name('checkCode');

Route::GET('/complains', 'ComplainController@index')->name('indexComplain');
Route::GET('/complains/create/{agent}', 'ComplainController@create')->name('agentCreate');
Route::POST('/complains', 'ComplainController@store')->name('agentStore');

Route::GET('/agentPage', 'AgentController@indexAgentPages')->name('indexAgentPages');
Route::GET('/agentPage/{agent}', 'AgentController@agentPageShow')->name('agentPage');
// Route::get('/home', 'HomeController@index')->name('home');

Route::GET('/test', 'PaymentController@index');

