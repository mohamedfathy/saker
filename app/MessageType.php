<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageType extends Model
{
	protected $table = 'messages_types';
    public $timestamps = false;
}
