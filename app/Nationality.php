<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
	protected $table = 'nationalites';
    public $timestamps = false;
}
