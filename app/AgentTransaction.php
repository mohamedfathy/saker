<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentTransaction extends Model
{
	protected $table = 'agents_transactions';
    public $timestamps = false;
}
