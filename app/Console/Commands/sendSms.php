<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Agent;
use App\AppSetting;
use App\AgentPayment;
use Carbon\Carbon;
use Auth;

class sendSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendSms:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send sms to agent in 28 every month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $AppSetting = AppSetting::find(1);
        $AlertDay = $AppSetting->payment_alerting_day;
        
        $TodayDay = date("d");
        $month = date("Y-m-00");
        if ($AlertDay == $TodayDay) {
            $AgentPayments = AgentPayment::where('month', $month)->get();
            
            foreach ( $AgentPayments as $AP) {
                $Agent = Agent::find($AP->agent_id);
                $this->sendSMS($Agent->id, $Agent->phone,'.الرجاء زيارتنا لاستلام مستحقاتك المالية بحد اقصى يوم 1 فى الشهر تجنبا لفقدان مستحقاتك');
            }
        }
    }

    protected function sendSMS($id, $phone, $msg){
        
    	//GuzzleHttp\Client
        $client = new \GuzzleHttp\Client(); 

	    $DateTime = date("Y-m-d H:i:s");
	    $baseURL = "https://www.smartsmsgateway.com/api/api_http.php?username=saqirtasheel&password=ast987&senderid=ALSAQIR SMS&to={$phone}&text=". urlencode($msg). "&type=unicode&datetime={$DateTime}";

	    $Handel = $client->get($baseURL);
	    
        //Check if sms send to agent
        $Sent = strpos($Handel,"OK");
        
        if( $Sent !== false ) {
            $AgentMessage = new AgentMessage();
            $AgentMessage->agent_id = $id;
            $AgentMessage->message_id = 1;
            $AgentMessage->admin_id = Auth::user()->id;
            $AgentMessage->created_at = $DateTime;
            $AgentMessage->save();
        }

    }
}
