<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Transaction;
use App\AgentPayment;

class migrateTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrateTransactions:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to calculate agent payments. and move them from transactions table to agents_payments table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Year  = date("Y");
        $Month  = date("m");
        $Date = date("Y-m-1");
        
        $Tranactions = Transaction::whereYear('created_at', $Year)
                            ->whereMonth('created_at', $Month )
                            ->where('is_successful', 1)
                            ->where('migrated', 0)
                            ->get();
                            
        $AgentsArray = [];                    
        foreach($Tranactions as $Tranaction) {
            $AgentsArray[] = $Tranaction->agent_id;
        }
        
        $Agents = array_unique($AgentsArray);
        

        foreach ($Agents as $Agent) {
            // Get all the transactions 
            $AgentTranactions = Transaction::whereYear('created_at', $Year)
                    ->whereMonth('created_at', $Month )
                    ->where('is_successful', 1)
                    ->where('migrated', 0)
                    ->where('agent_id', $Agent)
                    ->get();
                    
            // initialize the cost variable with Zero   
            $Cost = 0;
            
            // Add the cost to Agent
            foreach($AgentTranactions as $AT) {
                $Cost += $AT->cost;
            }
            
            // Create New Record for each agent for payment this Month
            $AgentPayment = new AgentPayment();
            $AgentPayment->month = $Date;
            $AgentPayment->total = $Cost;
            $AgentPayment->is_paid = 0;
            $AgentPayment->is_late = 0;
            $AgentPayment->agent_id = $AT->agent_id;
            $AgentPayment->save();
            
            
            // Get all the transactions and mark them as Migrated 
            // migration means moving the date form transactions to agentPayments tables;
            $MigratedTranactions = Transaction::whereYear('created_at', $Year)
                    ->whereMonth('created_at', $Month )
                    ->where('is_successful', 1)
                    ->where('migrated', 0)
                    ->where('agent_id', $Agent)
                    ->get();
                    
            foreach($MigratedTranactions as $MT) {
                $MT->migrated = 1;
                $MT->save();
            }
        }
    }
}
