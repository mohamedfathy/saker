<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AgentPayment;
use App\LatePayment;

class latePayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'latePayments:delay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Is Used to set is_late=true ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $Year  = date("Y");
        $Month  = date("m");
        $Date = date("Y-m-1");
        
        $LatePayments = AgentPayment::whereYear('month', $Year)
                                    ->whereMonth('month', $Month )
                                    ->where('is_paid', 0)
                                    ->where('migrated', 0)
                                    ->get();
                                    
        foreach ($LatePayments as $LatePayment) {
            $LatePaymentObject =  new LatePayment();
            $LatePaymentObject->agent_id = $LatePayment->agent_id;
            $LatePaymentObject->month = $LatePayment->month;
            $LatePaymentObject->cost = $LatePayment->total;
            $LatePaymentObject->receive_date = null;
            $LatePaymentObject->received = 0;
            $LatePaymentObject->save();
            
            $LatePayment->is_late = 1;
            $LatePayment->migrated = 1;
            $LatePayment->save();
        }
                            
    }
}
