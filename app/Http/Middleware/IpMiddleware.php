<?php

namespace App\Http\Middleware;

use Closure;
use App\UserBlocking;

class IpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $ipsDeny = UserBlocking::where('ip',$request->ip())->first();
        
        if($ipsDeny == NULL){
            return $next($request);
        }else{
            \Log::warning("Unauthorized access, IP address was => ".request()->ip);
                // return abort(500,'Unauthorized access,Your ip address is blocked from Administrator');
                return response("Unauthorized access,Your ip address is blocked from Administrator", 500);
        }
        
        
    }
}
