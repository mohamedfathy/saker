<?php

namespace App\Http\Middleware;

use Closure;
use App\Admin;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    $admin=Admin::where('id',\Auth::user()->id)->first();
    
    if($admin->role_id == 1){
        return $next($request);
        }else{
        return viwe('unauth');
        }
    }
}
