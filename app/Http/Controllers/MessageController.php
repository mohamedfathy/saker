<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Language;
use App\MessageType;
use Exception; 

class MessageController extends Controller
{
    private $messages = [
        'required' => 'يجب ادخال قيمة',
        'numeric' => 'يجب ادخال قيمة رقمية',
        'boolean' => 'يجب ادخال قيمة منطقية',
        'unique' => 'يجب ادخال قيمة مختلفه',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Message = Message::all();
        return view('messages.index', compact('Message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Language = Language::all();
        $MessageType = MessageType::all();
        return view('messages.create', compact('Language', 'MessageType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'message_type_id' => "required",
            'language_id' => "required",
            'content' => "required",
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);
 
        try {
            $Message = new Message();
            $Message->message_type_id = $request->message_type_id;
            $Message->language_id = $request->language_id;
            $Message->content = $request->content;
            $Message->save();
        } catch (Exception $e) {
            if ($e->errorInfo[0] === "23000") {
                echo "من فضلك .. ادخل قيم مختلفة لكل من نوع الرسالة و اللغة  <br /> <a href=\"\">لوحة التحكم</a>";
            }
        }
        return redirect('/message');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Message = Message::find($id);
        $Language = Language::all();
        $MessageType = MessageType::all();
        return view('messages.edit', compact('Message', 'Language', 'MessageType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'message_type_id' => "required",
            'language_id' => "required",
            'content' => "required",
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);

        $Message = Message::find($request->id);
        
        $Message->message_type_id = $request->message_type_id;
        $Message->language_id = $request->language_id;
        $Message->content = $request->content;
        $Message->save();

        return redirect('/message');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $MessageType = message::destroy($id);
        return back();
    }
}
