<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Role;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    private $messages = [
        'required' => 'يجب ادخال رقمية',
        'numeric' => 'يجب ادخال قيمة رقمية',
        'boolean' => 'يجب ادخال قيمة منطقية',
        'unique' => 'يجب ادخال قيمة مختلفه',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Admin = Admin::all();
        return view('admins.index', compact('Admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Role = Role::all();
        return view('admins.create', compact('Role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => "required|min:3|max:50",
            'second_name' => "required|min:3|max:50",
            'third_name' => "required|min:3|max:50",
            'email' => "required|unique:admins,email",
            'password' => 'required|confirmed',
            'photo_url' => 'required|image|mimes:jpeg,jpg,png|max:1000',
            'national_number' => ['required',Rule::unique('admins')],
            'role_id' => "required",
            'is_active' => "required|boolean",
            'job'=> "required|unique:admins,number_job"
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);

        $date = date('Y-m-d');

        if ($request->file('photo_url') == true) {
            $photo_url = $request->photo_url;
            $photo_url_photo = str_random(30) . '.' . $photo_url->getClientOriginalExtension();
            $photo_url->move(public_path('uploads/agents'), $photo_url_photo);
            $full_path_photo_url = Request()->root() . '/uploads/agents/' . $photo_url_photo;
        }
        $Admin = new Admin();
        $Admin->first_name = $request->first_name;
        $Admin->second_name = $request->second_name;
        $Admin->third_name = $request->third_name;
        $Admin->email = $request->email;
        $Admin->password = Hash::make($request->password);
        if ($request->file('photo_url')) {
            $Admin->photo_url = $full_path_photo_url;
        }
        $Admin->national_number = $request->national_number;
        $Admin->role_id = $request->role_id;
        $Admin->is_active = $request->is_active;
        $Admin->created_at = $date;
        $Admin->number_job=$request->job;
        $Admin->save();

        return redirect('/admins');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Admin = Admin::find($id);
        return view('admins.show', compact('Admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Admin = Admin::find($id);
        $Role = Role::all();
        return view('admins.edit', compact('Admin', 'Role'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'first_name' => "required|min:3|max:50",
            'second_name' => "required|min:3|max:50",
            'third_name' => "required|min:3|max:50",
            'email' => ['required', 'max:100', Rule::unique('admins')->ignore($request->id)],
            'password' => 'nullable|confirmed',
            'photo_url' => 'nullable|image|image|mimes:jpeg,jpg,png|max:1000',
            'national_number' => 'required',
            // 'national_number' => ['required',Rule::unique('admins')],
            'role_id' => "required",
            'is_active' => "required|boolean",
            'job'=> "required|unique:admins,number_job,id".$id
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);

        if ($request->file('photo_url') == true) {
            $photo_url = $request->photo_url;
            $photo_url_photo = str_random(30) . '.' . $photo_url->getClientOriginalExtension();
            $photo_url->move(public_path('uploads/admins'), $photo_url_photo);
            $full_path_photo_url = Request()->root() . '/uploads/admins/' . $photo_url_photo;
        }

        $Admin = Admin::find($request->id);
        $Admin->first_name = $request->first_name;
        $Admin->second_name = $request->second_name;
        $Admin->third_name = $request->third_name;
        $Admin->email = $request->email;
        if ($request->has('password')) {
            $Admin->password = Hash::make($request->password);
        }
        if ($request->file('photo_url')) {
            $Admin->photo_url = $full_path_photo_url;
        }
        $Admin->national_number = $request->national_number;
        $Admin->role_id = $request->role_id;
        $Admin->is_active = $request->is_active;
          $Admin->number_job=$request->job;
        $Admin->save();

        return redirect('/admins');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Admin = Admin::destroy($id);
        return back();
    }

    /**
     * Activate or Deactivate an Agent
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activation($id)
    {
        $Admin = Admin::find($id);
        if ($Admin->is_active == 1) {
            $Admin->is_active = 0;
        } else { $Admin->is_active = 1;}
        $Admin->save();
        return redirect('/admins');
    }
}
