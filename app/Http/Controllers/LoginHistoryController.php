<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoginHistory;
use App\Admin;

class LoginHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Admin = Admin::all();
        $logs = LoginHistory::all();
        return view('logins.index', compact('Admin', 'logs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $log = LoginHistory::destroy($id);
        return back();
    }

    /**
     * Activate or Deactivate an admin ip
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activation($id)
    {
        $log = LoginHistory::find($id);
        if ($log->paned == 1) {
            $log->paned = 0;
        } else { $log->paned = 1;}
        $log->save();
        return redirect('/logins');
    }
}
