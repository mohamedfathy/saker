<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\AgentPayment;
use App\Agent;

class AgentPaymentController extends Controller
{

    private $messages = [
        'required'=>'يجب ادخال قيمة',
        'numeric'=>'يجب ادخال قيمة رقمية',
        'boolean'=>'يجب ادخال قيمة منطقية',
        'unique'=>'يجب ادخال قيمة مختلفه'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $Agent = Agent::all();
        return view('agentPayments.index', compact('Agent'));
    }
    
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $AgentPayment = AgentPayment::where('agent_id', $id)->get();
        return view('agentPayments.show', compact('AgentPayment'));
    }
    
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Agent = Agent::all();
        $Nationality = Nationality::all();
        $companies = Company::all();
        return view('agents.create', compact('Agent', 'Nationality', 'companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name'=>"required|min:3|max:50",
            'last_name'=>"required|min:3|max:50",
            'thired_name'=>"required|min:3|max:50",
            'national_number'=>'required|unique:agents,national_number|max:15',
            'phone'=>'required|unique:agents,phone|max:15',
            'age' => "required",
            'email'=>'required|unique:agents,email',
            'nationality_id'=>'required',
            'barcode' => "required",
            'photo_url' => "required|image|mimes:jpeg,jpg,png|max:1000",
            'is_active'=>"required|boolean",
            'color'=>"required"
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);

        $date = date('Y-m-d');

        if($request->file('photo_url') == true){
        $photo_url = $request->photo_url;
        $photo_url_photo = str_random(30) . '.' . $photo_url->getClientOriginalExtension();
        $photo_url->move( public_path('uploads/agents') , $photo_url_photo);
        $full_path_photo_url = Request()->root().'/uploads/agents/'.$photo_url_photo;
        }
        $Agent = new Agent();
        $Agent->first_name = $request->name;
        $Agent->last_name = $request->last;
        $Agent->thired_name = $request->thired;
        $Agent->national_number = $request->national_number;
        $Agent->phone = $request->phone;
        $Agent->age = $request->age;
        $Agent->email = $request->email;
        $Agent->nationality_id = $request->nationality_id;
        $Agent->barcode = $request->barcode;
        if($request->file('photo_url')) { 
            $Agent->photo_url = $full_path_photo_url; 
        }
        $Agent->is_active = $request->is_active;
        $Agent->created_at = $date;
        $Agent->color = $request->color;
        $Agent->save();
        
        
		if($request->company_number){
		foreach($request->company_number as $company_number ){
            $agent_company = new AgentCompany;
            $agent_company->company_id = $company_number;
            $agent_company->agent_id = $Agent->id;
            $agent_company->save();
        }
}
        return redirect('/agents');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Agent = Agent::find($id);
        $Nationality = Nationality::all();
        return view('agents.edit', compact('Agent', 'Nationality'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {     
        $rules = [
            'first_name'=>"required|min:3|max:50",
            'last_name'=>"required|min:3|max:50",
            'thired_name'=>"required|min:3|max:50",
            'national_number'=>['required',Rule::unique('agents')->ignore($request->id),],
            'phone'=>['required','max:15',Rule::unique('agents')->ignore($request->id),],
            'age' => "required",
            'email'=>['required','max:100',Rule::unique('agents')->ignore($request->id),],
            'nationality_id'=>"required",
            'barcode' => "required",
            'photo_url' => "nullable|image|mimes:jpeg,jpg,png|max:1000'",
            'is_active'=>"required|boolean",
            'color'=>"required"
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);
   
        if($request->file('photo_url') == true){
        $photo_url = $request->photo_url;
        $photo_url_photo = str_random(30) . '.' . $photo_url->getClientOriginalExtension();
        $photo_url->move( public_path('uploads/agents') , $photo_url_photo);
        $full_path_photo_url = Request()->root().'/uploads/agents/'.$photo_url_photo;
        }

        $Agent = Agent::find($request->id);
        $Agent->first_name = $request->first_name;
        $Agent->last_name = $request->last_name;
        $Agent->thired_name = $request->thired_name;
        $Agent->national_number = $request->national_number;
        $Agent->phone = $request->phone;
        $Agent->age = $request->age;
        $Agent->email = $request->email;
        $Agent->nationality_id = $request->nationality_id;
        $Agent->barcode = $request->barcode;
        if($request->file('photo_url')) { 
            $Agent->photo_url = $full_path_photo_url; 
        }
        
        $Agent->is_active = $request->is_active;
        $Agent->color = $request->color;
        $Agent->save();

        return redirect('/agents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Agent = Agent::destroy($id);
        return back();
    }
    
    /**
    * Activate or Deactivate an Agent
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function agentGetPayed ($id){
        $AgentPayment = AgentPayment::find($id);
        if($AgentPayment->is_paid == 1){
            $AgentPayment->is_paid = 0;
        }
        else{$AgentPayment->is_paid = 1;}
        $AgentPayment->save();
        return back();
    } 
}
