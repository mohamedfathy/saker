<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Foundation\Auth;
use App\AgentMessage;
use App\MessageType;
use App\Agent;
use App\Admin;
use App\User;
use App\Message;
use Auth;

class AgentMessageController extends Controller
{

    private $messages = [
        'required'=>'يجب ادخال قيمة',
        'numeric'=>'يجب ادخال قيمة رقمية',
        'boolean'=>'يجب ادخال قيمة منطقية',
        'unique'=>'يجب ادخال قيمة مختلفه'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $AgentMessage = AgentMessage::all();
        $balance=$this->getBalance();
        return view('agentMessages.index', compact('AgentMessage','balance'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $MessageType = MessageType::all();
        $Messages = Message::all();
        $Agent = Agent::all();
        $Admin = Admin::all();
        return view('agentMessages.create', compact('MessageType', 'Agent', 'Admin', 'Messages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'message_id'=>"required",
            'agent_id'=>"required|array",
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);
        
        $Message = Message::find($request->message_id);
        
        $date = date('Y-m-d');
        
        foreach ($request->agent_id as $agent_id) {
            //get Agent Phone
            $Agent = Agent::where('id', $agent_id)->first(); 
        
            //send sms To phone Agent
            $Handel = $this->sendSMS($Agent->phone, $Message->content);
            
            //Check if sms send to agent
            $Sent =strpos($Handel,"OK");
            
            $messages = [];
            if ( $Sent === false ) {
                $messages[] = "خطا فى ارسال الرسائل";
        	    return Redirect::back()->withInput()->with('sendMessageFalid', $messages);
            } else {
            
                $AgentMessage = new AgentMessage();
                $AgentMessage->agent_id = $agent_id;
                $AgentMessage->message_id = $request->message_id;
                $AgentMessage->admin_id = Auth::user()->id;
                $AgentMessage->created_at = $date;
                $AgentMessage->save();
                 $messages[] = "تم ارسال الرسائل بنجاح";
                //return redirect('/agentMessages')->with('sendMessageSucc','تم ارسال الرسالة بنجاح ');
            }
        }
        return redirect('/agentMessages')->with('sendMessageSucc', $messages);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $AgentMessage = AgentMessage::find($id);
        $MessageType = MessageType::all();
        $Agent = Agent::all();
        $Admin = Admin::all();
        return view('agentMessages.edit', compact('MessageType', 'Agent', 'Admin', 'AgentMessage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'message_type_id'=>"required",
            'agent_id'=>"required",
            // 'admin_id'=>"required",
            'content'=>"required|min:3|max:200",
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);

        $date = date('Y-m-d');
        //get Agent Phone
        $Agent=Agent::where('id',$request->agent_id)->first(); 
        //send sms To phone Agent
        $x=$this->sendSMS($Agent->phone,$request->content);
        //Check if sms send to agent
        $check_ok=strpos($x,"OK");

    if($check_ok === false ){
 
        return Redirect::back()->withInput()->with('sendMessageFalid','خطاء في ارسال الرسالة ');
 }else{
        $AgentMessage = AgentMessage::find($request->id);
        $AgentMessage->message_type_id = $request->message_type_id;
        $AgentMessage->agent_id = $request->agent_id;
        $AgentMessage->admin_id = Auth::user()->id;
        // $AgentMessage->admin_id = $request->admin_id;
        $AgentMessage->content = $request->content;
        $AgentMessage->save();

         return redirect('/agentMessages')->with('sendMessageSucc','تم ارسال الرسالة بنجاح ');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $AgentMessage = AgentMessage::destroy($id);
        return back();
    }
}
