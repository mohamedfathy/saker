<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserBlocking;
class UserBlockingController extends Controller
{
    private $messages = [
        'required' => 'يجب ادخال قيمة',
        'numeric' => 'يجب ادخال قيمة رقمية',
        'boolean' => 'يجب ادخال قيمة منطقية',
        'unique' => 'يجب ادخال قيمة مختلفه',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $BlockedUser = UserBlocking::all();
        return view('blockedUsers.index', compact('BlockedUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blockedUsers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'ip' => "required",
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);

        $UserBlocking = new UserBlocking();
        $UserBlocking->ip = $request->ip;
        $UserBlocking->admin_id = 1;

        $UserBlocking->save();

        return redirect('/blockedUsers');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $UserBlocking = UserBlocking::find($id);
        return view('blockedUsers.edit', compact('UserBlocking'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'ip' => "required",
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);


        $UserBlocking = UserBlocking::find($request->id);
        $UserBlocking->ip = $request->ip;
        $UserBlocking->admin_id = 1;
        $UserBlocking->save();

        return redirect('/blockedUsers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $UserBlocking = UserBlocking::destroy($id);
        return back();
    }

    /**
     * Activate or Deactivate an Agent
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activation($id)
    {
        $Admin = Admin::find($id);
        if ($Admin->is_active == 1) {
            $Admin->is_active = 0;
        } else { $Admin->is_active = 1;}
        $Admin->save();
        return redirect('/admins');
    }
}
