<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Agent;
use App\Nationality;
use App\Company;
use App\AgentCompany;
use App\Transaction;
use App\AgentTransaction;
use DB;
use App\Payment;
use App\AgentMessage;
use \stdClass;
use Carbon\Carbon;


class AgentController extends Controller
{

    private $messages = [
        'required'=>'يجب ادخال قيمة',
        'numeric'=>'يجب ادخال قيمة رقمية',
        'boolean'=>'يجب ادخال قيمة منطقية',
        'unique'=>'يجب ادخال قيمة مختلفه',
        'regex' => 'يجب ان يبدا الرقم ب 00971',
        'digits_between' => 'يجب الا تزيد و لا تقل عدد الارقام عن 14 رقم',
        'exists'=>'غير موجود بقاعدة البيانات'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Agent = Agent::all();
        return view('agents.index', compact('Agent'));
    }
    
    public function indexAgentPages()
    {
        $Agent = Agent::all();
        return view('agentPage.agentPageIndex', compact('Agent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Agent = Agent::all();
        $Nationality = Nationality::all();
        $companies = Company::all();
        return view('agents.create', compact('Agent', 'Nationality', 'companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $rules = [
            'name'=>"required|min:3|max:50",
            'last'=>"required|min:3|max:50",
            'thired'=>"required|min:3|max:50",
            'national_number'=>'required|unique:agents,national_number|max:15',
            'phone'=>'required|unique:agents,phone|max:15',
            'age' => "required",
            'email'=>'required|unique:agents,email',
            'nationality_id'=>'required',
            'barcode' => "required",
            'photo_url' => "required|image|mimes:jpeg,jpg,png|max:1000",
            'is_active'=>"required|boolean",
            'color'=>"required",
            'card_number'=>"required"
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);

        $date = date('Y-m-d');

        if($request->file('photo_url') == true){
        $photo_url = $request->photo_url;
        $photo_url_photo = str_random(30) . '.' . $photo_url->getClientOriginalExtension();
        $photo_url->move( public_path('uploads/agents') , $photo_url_photo);
        $full_path_photo_url = Request()->root().'/uploads/agents/'.$photo_url_photo;
        }
        $Agent = new Agent();
        $Agent->first_name = $request->name;
        $Agent->last_name = $request->last;
        $Agent->thired_name = $request->thired;
        $Agent->national_number = $request->national_number;
        $Agent->phone = $request->phone;
        $Agent->age = $request->age;
        $Agent->email = $request->email;
        $Agent->nationality_id = $request->nationality_id;
        $Agent->barcode = $request->barcode;
        $Agent->card_number = $request->card_number;
        if($request->file('photo_url')) { 
            $Agent->photo_url = $full_path_photo_url; 
        }
        $Agent->is_active = $request->is_active;
        $Agent->created_at = $date;
        $Agent->color = $request->color;
        $Agent->save();
        
        
		if($request->company_number){
          foreach($request->company_number as $company_number ){
              $agent_company = new AgentCompany;
              $agent_company->company_id = $company_number;
              $agent_company->agent_id = $Agent->id;
              $agent_company->save();
          }
		}
        return redirect('/agents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Agent = Agent::find($id);
        $transactions = Transaction::all();
        $agetrans = AgentTransaction::all();
        
        $UserCompaniesObjects = AgentCompany::where('agent_id', $id)->get();
        $UserCompanies = []; 
        foreach ($UserCompaniesObjects as $UCO) {
        	$UserCompanies[] = $UCO->company_id;
        }
        $Companies = []; 
        foreach ($UserCompanies as $UC) {
        	$Companies[] = Company::find($UC)->get();
        }
        //dd($Companies);
        return view('agents.show', compact('Agent','transactions','agetrans', 'Companies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Agent = Agent::find($id);
        $Nationality = Nationality::all();
        $Companies = Company::all();
        $UserCompanies = Company::all();
        $UserCompaniesObjects = AgentCompany::where('agent_id', $id)->get();
        $UserCompanies = []; 
        foreach ($UserCompaniesObjects as $UCO) {
        	$UserCompanies[] = $UCO->company_id;
        }
        return view('agents.edit', compact('Agent', 'Nationality', 'Companies', 'UserCompanies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {     
        $rules = [
            'first_name'=>"required|min:3|max:50",
            'last_name'=>"required|min:3|max:50",
            'thired_name'=>"required|min:3|max:50",
            'national_number'=>['required',Rule::unique('agents')->ignore($request->id),],
            'phone'=>['required','max:15',Rule::unique('agents')->ignore($request->id),],
            'age' => "required",
            'email'=>['required','max:100',Rule::unique('agents')->ignore($request->id),],
            'nationality_id'=>"required",
            'barcode' => "required",
            'photo_url' => "nullable|image|mimes:jpeg,jpg,png|max:1000'",
            'is_active'=>"required|boolean",
            'color'=>"required",
            'card_number'=>"required"
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);
   
        if($request->file('photo_url') == true){
        $photo_url = $request->photo_url;
        $photo_url_photo = str_random(30) . '.' . $photo_url->getClientOriginalExtension();
        $photo_url->move( public_path('uploads/agents') , $photo_url_photo);
        $full_path_photo_url = Request()->root().'/uploads/agents/'.$photo_url_photo;
        }

        $Agent = Agent::find($request->id);
        $Agent->first_name = $request->first_name;
        $Agent->last_name = $request->last_name;
        $Agent->thired_name = $request->thired_name;
        $Agent->national_number = $request->national_number;
        $Agent->phone = $request->phone;
        $Agent->age = $request->age;
        $Agent->email = $request->email;
        $Agent->nationality_id = $request->nationality_id;
        $Agent->barcode = $request->barcode;
        $Agent->card_number = $request->card_number;
        if($request->file('photo_url')) { 
            $Agent->photo_url = $full_path_photo_url; 
        }
        
        $Agent->is_active = $request->is_active;
        $Agent->color = $request->color;
        $Agent->save();

        return redirect('/agents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Agent = Agent::destroy($id);
        return back();
    }
    
    /**
    * Activate or Deactivate an Agent
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function activation ($id){
        $Agent = Agent::find($id);
        if($Agent->is_active == 1){
        $Agent->is_active = 0;
        }
        else{$Agent->is_active = 1;}
        $Agent->save();
        return redirect('/agents');
    } 
    
    
    public function agentPageShow (Request $request, $id)
    {
    	if (isset($request->month)) {
        	$monthDate = $request->month;
        } else {
        	$monthDate = date('m');
        }
    	
        $Agent = Agent::find($id);
        $joinDate = $Agent->created_at;
        $TransactionCount = Transaction::where('agent_id', $id)
                                    ->count();
                                    
        $Transaction = Transaction::where('agent_id', $id)
        							->whereMonth('created_at', $monthDate)
                                    ->get();
        $successTrans = Transaction::where('is_successful', 1)
                					->whereMonth('created_at', $monthDate)
                                    ->where('agent_id', $id)
                                    ->count();
        $deservedMoney = Transaction::where('is_successful', 1)
        							  ->whereMonth('created_at', $monthDate)
                                      ->where('agent_id', $id)
                                      ->sum('cost');
        
        return view('agentPage.agentPageShow', compact('Agent', 'Transaction', 'monthDate', 'successTrans', 'deservedMoney', 'joinDate', 'TransactionCount'));
    }
    
    public function agentPageSearch (Request $request, $id)
    {
    	if (isset($request->month)) {
        	$monthDate = $request->month;
        } else {
        	$monthDate = date('m');
        }
    	
        $Agent = Agent::find($id);
        $Transaction = Transaction::where('agent_id', $id)
        							->whereMonth('created_at', $monthDate)
                                    ->get();
        $successTrans = Transaction::where('is_successful', 1)
                					->whereMonth('created_at', $monthDate)
                                    ->where('agent_id', $id)
                                    ->count();
        $deservedMoney = Transaction::where('is_successful', 1)
        							  ->whereMonth('created_at', $monthDate)
                                      ->where('agent_id', $id)
                                      ->sum('cost');
        
        return view('report.agentReportShow', compact('Agent', 'Transaction', 'monthDate', 'successTrans', 'deservedMoney'));
    }
    
    public function phonePage()
    {
        return view('agentPage.login');
    }
    
    public function checkphone(Request $request)
    {
    	$rules = [
            'phone'=>"required|digits_between:14,14|regex:/(00971)[0-9]{9}/|exists:agents,phone",
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);
        
        
        // find the agent
    	$Agent = Agent::where('phone', $request->phone)->first();
		
        // send the code to the user 
        $code = mt_rand(100000, 999999);
        $Agent->code = $code;
        $Agent->save();
        
        $x=$this->sendSMS($Agent->phone,$code);
        //Check if sms send to agent
        $check_ok=strpos($x,"OK");
        
        // get redirected to code page
        return redirect('/agentCode');
    }
    
    public function codePage()
    {
        return view('agentPage.code');
    }
    
    public function checkCode(Request $request)
    {
    
    	$rules = [
           'code'=>"exists:agents,code",
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);
        
        
        // find the agent
    	$Agent = Agent::where('code', $request->code)->first();
     
		//$Agent = null;
		if ($Agent !== null ) {
        	// success
            return redirect("/agentPage/$Agent->id");
        } else {
        	// failure 
            return back();
        }
    }
    
}
