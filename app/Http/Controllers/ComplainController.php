<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Complain;
class ComplainController extends Controller
{
    private $messages = [
        'required' => 'يجب ادخال قيمة',
        'numeric' => 'يجب ادخال قيمة رقمية',
        'boolean' => 'يجب ادخال قيمة منطقية',
        'unique' => 'يجب ادخال قيمة مختلفه',
    ];
    
    public function index () {
    	$Complain = Complain::all();
    	return view('complain.index', compact('Complain'));
    }

    public function destroy ($id) {
    	$Complain = Complain::destroy($id);
        return back();
    }
    

    public function create($id)
    {
        return view('complain.create', compact('id'));
    }

    public function store(Request $request)
    {
    
        $rules = [
            'id'=>"required|numeric",
            'message'=>"required",
            'suggestion'=>"nullable",
            'rating'=>'required|numeric',
        ];

        // vaildate the form 
        $this->validate($request, $rules, $this->messages);

        $date = date('Y-m-d');


        $Complain = new Complain();
        $Complain->message = $request->message;
        $Complain->rating	 = $request->rating;
        $Complain->suggestion = $request->suggestion;
        $Complain->created_at = $date;
        $Complain->agent_id = $request->id;
        $Complain->save();
        

        return redirect('/agents');
    }

}
