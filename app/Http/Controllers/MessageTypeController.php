<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MessageType;

class MessageTypeController extends Controller
{   
    private $messages = [
        'required' => 'يجب ادخال قيمة',
        'numeric' => 'يجب ادخال قيمة رقمية',
        'boolean' => 'يجب ادخال قيمة منطقية',
        'unique' => 'يجب ادخال قيمة مختلفه',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $MessageType = MessageType::all();
        return view('messageType.index', compact('MessageType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('messageType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => "required",
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);

        $MessageType = new MessageType();
        $MessageType->name = $request->name;

        $MessageType->save();

        return redirect('/messagType');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $MessageType = MessageType::find($id);
        return view('messageType.edit', compact('MessageType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|min:3|max:50",
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);


        $MessageType = MessageType::find($request->id);
        $MessageType->name = $request->name;
        $MessageType->save();

        return redirect('/messagType');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $MessageType = MessageType::destroy($id);
        return back();
    }
}
