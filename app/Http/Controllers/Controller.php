<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use DB;
use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /*
    * @param int phone
    * @param string message
    * @return status
    */
    protected function sendSMS($phone, $msg){
        
    	//GuzzleHttp\Client
        $client = new \GuzzleHttp\Client(); 

	    $DateTime = date("Y-m-d H:i:s");
	    $baseURL = "https://www.smartsmsgateway.com/api/api_http.php?username=saqirtasheel&password=ast987&senderid=ALSAQIR SMS&to={$phone}&text=". urlencode($msg). "&type=unicode&datetime={$DateTime}";

	    $result = $client->get($baseURL);
   	    return  $result->getBody()->getContents();
    }
  
    public function getBalance(){
        $client = new \GuzzleHttp\Client(); //GuzzleHttp\Client
        $result = $client->post('https://www.smartsmsgateway.com/api/api_http_balance.php?', [
            'form_params' => [
             'username' 	=> 'saqirtasheel',
             "password"	    => "ast987",
                             ]
        ]);
       return  intval($result->getBody()->getContents());
    }

  
}
