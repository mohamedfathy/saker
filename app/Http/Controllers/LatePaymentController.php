<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LatePayment;

class LatePaymentController extends Controller
{
    public function index()
    {
        $LatePayment = LatePayment::all();
        return view('latePayment.index', compact('LatePayment'));
    }
    
    public function LatePaymentGetPayed($id)
    {
        $date = date('Y-m-d');
        $LatePayment = LatePayment::find($id);
        $LatePayment->received = 1;
        $LatePayment->receive_date = $date;
        $LatePayment->save();
        return back();
    }
    
}
