<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// use App\Charts;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AgentTransaction;
use App\Agent;
use App\Transaction;
use App\Message;
use App\AgentMessage;
use App\Payment;

// use Charts;

class chartController extends Controller
{
    public function chart() {

        $AgentsCount = Agent::count();
        $MessagesCount = AgentMessage::count();
        $TransactionsCount = Transaction::count();
        $PaymentsTotal = Payment::sum('cost');


        $AgentChart = Charts::multiDatabase('areaspline', 'highcharts')
        ->title('العملاء')
        ->colors(['#00acac', '#333333'])
        ->dataset('العملاء', Agent::all())
        ->elementLabel("عدد العملاء")
        ->dimensions(0, 0)
        ->responsive(true)
        ->lastByDay();

        $TransactionChart = Charts::multiDatabase('areaspline', 'highcharts')
        ->title('المعاملات')
        ->colors(['#00acac', '#333333'])
        ->dataset('المعاملات', Transaction::all())
        ->elementLabel("عدد المعاملات")
        ->dimensions(0, 0)
        ->responsive(true)
        ->lastByMonth(3,true);

        $MessageChart = Charts::multiDatabase('areaspline', 'highcharts')
        ->title('الرسائل')
        ->colors(['#00acac', '#333333'])
        ->dataset('الرسائل', AgentMessage::all())
        ->elementLabel("عدد الرسائل")
        ->dimensions(0, 0)
        ->responsive(true)
        ->lastByDay(10, true);

        $CostChart = Charts::multiDatabase('areaspline', 'highcharts')
        ->title('النفقات')
        ->colors(['#00acac', '#333333'])
        ->dataset('النفقات', Payment::all())
        ->elementLabel("اجمالى النفقات")
        ->dimensions(0, 0)
        ->responsive(true)
        ->lastByDay();

        return view('home.index', compact('AgentsCount', 'MessagesCount', 'TransactionsCount', 'PaymentsTotal', 'AgentChart', 'TransactionChart', 'MessageChart','CostChart'));
    }

    public function agentsDetails() {

        $todayDate = date('Y-m-d');
        $saturday = date( 'Y-m-d', strtotime( 'monday this week' ) );
		$friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
        $monthDate = date('m');
        $yearDate = date('Y');
        $day = date('d');

        $dailyUsers = Agent::whereDay('created_at', $day)->count();
        $weeklyUsers = Agent::whereBetween('created_at', [$saturday, $friday])->count();
        $monthlyUsers = Agent::whereMonth('created_at', $monthDate)->count();
        $yearlyUsers = Agent::whereYear('created_at', $yearDate)->count();

        return view('home.agentDetails', compact(
            'dailyUsers', 'weeklyUsers', 'monthlyUsers', 'yearlyUsers'
        ));
    }

    public function agentSearchDetails (Request $request) {
    
    	$from = $request->from;
        $to = $request->to;
        
        $SearchAgentCount = Agent::whereBetween('created_at', [$from, $to])->count();
        $SearchAgent = Agent::whereBetween('created_at', [$from, $to])->get();
    
        return view('home.agentSearchDetails', compact(
            'SearchAgentCount', 'SearchAgent', 'from', 'to'
        ));
    }

    public function messagesDetails() {

        $todayDate = date('Y-m-d');
        $saturday = date( 'Y-m-d', strtotime( 'monday this week' ) );
		$friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
        $monthDate = date('m');
        $yearDate = date('Y');
        $day = date('d');

        $dailyMessages = AgentMessage::whereDay('created_at', $day)->count();
        $weeklyMessages = AgentMessage::whereBetween('created_at', [$saturday, $friday])->count();
        $monthlyMessages = AgentMessage::whereMonth('created_at', $monthDate)->count();
        $yearlyMessages = AgentMessage::whereYear('created_at', $yearDate)->count();

        return view('home.messageDetails', compact(
            'dailyMessages', 'weeklyMessages', 'monthlyMessages', 'yearlyMessages'
        ));
    }

    public function messagesSearchDetails (Request $request) {
    
    	$from = $request->from;
        $to = $request->to;
        
        $SearchMessagesCount = AgentMessage::whereBetween('created_at', [$from, $to])->count();
        $SearchMessages = AgentMessage::whereBetween('created_at', [$from, $to])->get();
    
        return view('home.messageSearchDetails', compact(
            'SearchMessagesCount', 'SearchMessages', 'from', 'to'
        ));
    }

    public function transactionsDetails() {

        $todayDate = date('Y-m-d');
        $saturday = date( 'Y-m-d', strtotime( 'monday this week' ) );
		$friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
        $monthDate = date('m');
        $yearDate = date('Y');
        $day = date('d');

        $dailyTransactions = Transaction::whereDay('created_at', $day)->count();
        $weeklyTransactions = Transaction::whereBetween('created_at', [$saturday, $friday])->count();
        $monthlyTransactions = Transaction::whereMonth('created_at', $monthDate)->count();
        $yearlyTransactions = Transaction::whereYear('created_at', $yearDate)->count();

        return view('home.transactionDetails', compact(
            'dailyTransactions', 'weeklyTransactions', 'monthlyTransactions', 'yearlyTransactions'
        ));
    }

    public function transactionsSearchDetails (Request $request) {
    
    	$from = $request->from;
        $to = $request->to;
        
        $SearchTransactionsCount = Transaction::whereBetween('created_at', [$from, $to])->count();
        $SearchTransactions = Transaction::whereBetween('created_at', [$from, $to])->get();
    
        return view('home.transactionSearchDetails', compact(
            'SearchTransactionsCount', 'SearchTransactions', 'from', 'to'
        ));
    }

    public function paymentsDetails() {

        $todayDate = date('Y-m-d');
        $saturday = date( 'Y-m-d', strtotime( 'monday this week' ) );
		$friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
        $monthDate = date('m');
        $yearDate = date('Y');
        $day = date('d');

        $dailyPayments = Payment::whereDay('created_at', $day)->where('is_late', 0)->sum('cost');
        $weeklyPayments = Payment::whereBetween('created_at', [$saturday, $friday])->where('is_late', 0)->sum('cost');
        $monthlyPayments = Payment::whereMonth('created_at', $monthDate)->where('is_late', 0)->sum('cost');
        $yearlyPayments = Payment::whereYear('created_at', $yearDate)->where('is_late', 0)->sum('cost');

        return view('home.paymentDetails', compact(
            'dailyPayments', 'weeklyPayments', 'monthlyPayments', 'yearlyPayments'
        ));
    }

    public function paymentsSearchDetails (Request $request) {
    
    	$from = $request->from;
        $to = $request->to;
        $SearchPaymentsCost = Payment::whereBetween('created_at', [$from, $to])->where('is_late', 0)->sum('cost');
        $SearchPayments = Payment::whereBetween('created_at', [$from, $to])->where('is_late', 0)->get();
    
        return view('home.paymentSearchDetails', compact(
            'SearchPaymentsCost', 'SearchPayments', 'from', 'to'
        ));
    }
}
