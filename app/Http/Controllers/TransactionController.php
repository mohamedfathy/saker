<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Agent;
use App\AppSetting;

class TransactionController extends Controller
{
    private $messages = [
        'required' => 'يجب ادخال قيمة',
        'numeric' => 'يجب ادخال قيمة رقمية',
        'boolean' => 'يجب ادخال قيمة منطقية',
        'unique' => 'يجب ادخال قيمة مختلفه',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Transaction = Transaction::all();
        return view('transactions.index', compact('Transaction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	 $Agent = Agent::all();
         return view('transactions.create', compact('Agent'));
    }
    
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation
        $this->validate($request, [
            'number' => 'required|max:100',
        ]);
		
        $date = date("Y-m-d H:i:s");
        
		$AppSetting = AppSetting::find(1);
		
        $Transaction = new Transaction;
        $Transaction->number = $request->number;
        $Transaction->created_at = $date;
        $Transaction->agent_id = $request->agent_id;
        $Transaction->is_successful = 1;
        $Transaction->cost = $AppSetting->comission_per_transaction;
        $Transaction->save();

        return redirect('/transactions');
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
     	$Agent = Agent::all();
        $Transaction = Transaction::find($id);
        return view('transactions.edit', compact('Transaction', 'Agent'));

     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'number' => "required|max:100",
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);


        $Transaction = Transaction::find($request->id);
        $Transaction->number = $request->number;
        $Transaction->save();

        return redirect('/transactions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Transaction = Transaction::destroy($id);
        return back();
    }


}
