<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppSetting;
use App\LoginHistory;
class AppSettingController extends Controller
{

    private $messages = [
        'required' => 'يجب ادخال قيمة',
        'numeric' => 'يجب ادخال قيمة رقمية',
        'boolean' => 'يجب ادخال قيمة منطقية',
        'unique' => 'يجب ادخال قيمة مختلفه',
    ];
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $AppSetting = AppSetting::all();
        return view('appSetting.index', compact('AppSetting'));
    }

    public function IndexLog()
    {
        $Log = LoginHistory::orderBy('id','DESC')->get();
     
        return view('appSetting.loginLog', compact('Log'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $AppSetting = AppSetting::find($id);
        return view('appSetting.edit', compact('AppSetting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $rules = [
            'comission_per_transaction' => "required",
            'payment_alerting_day' => "required",
            'allowed_payment_period' => "required",
        ];

        // vaildate the form
        $this->validate($request, $rules, $this->messages);

        $AppSetting = AppSetting::find($request->id);
        $AppSetting->comission_per_transaction = $request->comission_per_transaction;
        $AppSetting->payment_alerting_day = $request->payment_alerting_day;
        $AppSetting->allowed_payment_period = $request->allowed_payment_period;
        $AppSetting->save();

        return redirect('/appSetting');
    }

    public function import(Request $request)
    {
        $message = '';
        
        set_time_limit(0);

          if($_FILES["sql_url"]["name"] != '') {
            $array = explode(".", $_FILES["sql_url"]["name"]);
            $extension = end($array);
            
              if($extension == 'sql') {
                $connect = mysqli_connect(env("DB_HOST"), env("DB_USERNAME"), env("DB_PASSWORD"), env("DB_DATABASE"));
                $sconnect = mysqli_connect(env("DB_HOST"), env("DB_USERNAME"), env("DB_PASSWORD"));
                $drop = "DROP DATABASE saqara;";
                $handel = mysqli_query($sconnect, $drop);
                $create = "CREATE DATABASE saqara;";
                $handel = mysqli_query($sconnect, $create);
                $output = '';
                $count = 0;
                $file_data = file($_FILES["sql_url"]["tmp_name"]);

            
        foreach($file_data as $row) {
            $start_character = substr(trim($row), 0, 2);
            if($start_character != '--' || $start_character != '/*' || $start_character != '//' || $row != '')
            {
            $output = $output . $row;
            $end_character = substr(trim($row), -1, 1);
            if($end_character == ';')
            {
            if(!mysqli_query($connect, $output)) {
                $count++;
            }
            $output = '';
                }
                }
            }
            if($count > 0)
            {
                $message = '<label class="text-danger">There is an error in Database Import</label>';
                return $message;
            }else{ 
                $message = '<label class="text-success">Database Successfully Imported</label>';
                return $message; 
            }
            } else { 
                $message = '<label class="text-danger">Invalid File</label>';
                return $message;
            }
            } else {
            $message = '<label class="text-danger">Please Select Sql File</label>';
                return $message;
            }
    }

    public function export()
    {
		        //  $var =  getcwd ();
        $command = "mysqldump -u saqara -pNHFo8t0Hjx0p9iZL saqara > west.sql";
        exec($command);

        $myfile = fopen("west.sql", "r") or die("Unable to open file!");
        $txt = (fread($myfile,filesize("west.sql")));
    //dd($txt);
        $file_name = 'SaqaraBackup_' . date('20y-m-d ') . '.sql';

        $file_handle = fopen($file_name, 'w+');
        fwrite($file_handle, $txt);
        fclose($file_handle);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file_name));
        ob_clean();
        flush();
        readfile($file_name);
        unlink($file_name);
    }

}
