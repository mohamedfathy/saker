<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Language;


class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Language = Language::all();
        return view('languages.index', compact('Language'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validation
        $this->validate($request, [
            'name' => 'required|max:50|min:3',
        ]);

        $Language = new Language;
        $Language->name = $request->name;
        $Language->save();

        return redirect('/languages');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Language = Language::destroy($id);
        return back();
    }
}
