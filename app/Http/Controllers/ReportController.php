<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Transaction;
use App\AgentMessage;
use App\Agent;
use \stdClass;
use Carbon\Carbon;
class ReportController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($to=NULL,$from=NULL)
    {
    
        if($to   == NULL){  $to=Carbon::now(); }
        if($from == NULL) { $from=Carbon::now(); }
    
        $todayDate = date('Y-m-d');
        $monthDate = date('m');
        $yearDate = date('Y');
        $day = date('d');

        $dailyPayments = Payment::whereDay('created_at', $day)->where('is_late', 0)->sum('cost');
        $monthlyPayments = Payment::whereMonth('created_at', $monthDate)->where('is_late', 0)->sum('cost');
        $yearlyPayments = Payment::whereYear('created_at', $yearDate)->where('is_late', 0)->sum('cost');

        $dailyTransaction = Transaction::whereDay('created_at', $day)->count();
        $monthlyTransactions = Transaction::whereMonth('created_at', $monthDate)->count();
        $yearlyTransactions = Transaction::whereYear('created_at', $yearDate)->count();

        $dailyMessage = AgentMessage::whereDay('created_at', $day)->count();
        $monthlyMessages = AgentMessage::whereMonth('created_at', $monthDate)->count();
        $yearlyMessages = AgentMessage::whereYear('created_at', $yearDate)->count();

        
        $dailyPaymentsNotPayed = Payment::whereDay('created_at', $day)->where('is_late', 1)->sum('cost');
        $monthlyPaymentsNotPayed = Payment::whereMonth('created_at', $monthDate)->where('is_late', 1)->sum('cost');
        $yearlyPaymentsNotPayed = Payment::whereYear('created_at', $yearDate)->where('is_late', 1)->sum('cost');

        return view('report.index', compact(
            'dailyPayments', 'monthlyPayments', 'yearlyPayments',
            'dailyTransaction', 'monthlyTransactions', 'yearlyTransactions',
            'dailyMessage', 'monthlyMessages', 'yearlyMessages',
            'dailyPaymentsNotPayed', 'monthlyPaymentsNotPayed', 'yearlyPaymentsNotPayed',
            'from', 'to'
        ));
    }

    public function indexDetails () {
    
        $todayDate = date('Y-m-d');
        $saturday = date( 'Y-m-d', strtotime( 'monday this week' ) );
		$friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
        $monthDate = date('m');
        $yearDate = date('Y');
        $day = date('d');

        $dailyUsers = Agent::whereDay('created_at', $day)->count();
        $weeklyUsers = Agent::whereBetween('created_at', [$saturday, $friday])->count();
        $monthlyUsers = Agent::whereMonth('created_at', $monthDate)->count();
        $yearlyUsers = Agent::whereYear('created_at', $yearDate)->count();
        
        $dailyMessages = AgentMessage::whereDay('created_at', $day)->count();
        $weeklyMessages = AgentMessage::whereBetween('created_at', [$saturday, $friday])->count();
        $monthlyMessages = AgentMessage::whereMonth('created_at', $monthDate)->count();
        $yearlyMessages = AgentMessage::whereYear('created_at', $yearDate)->count();


        $dailyTransactions = Transaction::whereDay('created_at', $day)->count();
        $weeklyTransactions = Transaction::whereBetween('created_at', [$saturday, $friday])->count();
        $monthlyTransactions = Transaction::whereMonth('created_at', $monthDate)->count();
        $yearlyTransactions = Transaction::whereYear('created_at', $yearDate)->count();


        $dailyPayments = Payment::whereDay('created_at', $day)->where('is_late', 0)->sum('cost');
        $weeklyPayments = Payment::whereBetween('created_at', [$saturday, $friday])->where('is_late', 0)->sum('cost');
        $monthlyPayments = Payment::whereMonth('created_at', $monthDate)->where('is_late', 0)->sum('cost');
        $yearlyPayments = Payment::whereYear('created_at', $yearDate)->where('is_late', 0)->sum('cost');


        return view('details.index', compact(
            'dailyUsers', 'weeklyUsers', 'monthlyUsers', 'yearlyUsers',
            'dailyMessages', 'weeklyMessages', 'monthlyMessages', 'yearlyMessages',
            'dailyTransactions', 'weeklyTransactions', 'monthlyTransactions', 'yearlyTransactions',
            'dailyPayments', 'weeklyPayments', 'monthlyPayments', 'yearlyPayments',
            'form', 'to'
        ));

    }
    
    public function searchDetails (Request $request) {
    
    	$from = $request->from;
        $to = $request->to;
        
    	$searchUsers = Agent::whereBetween('created_at', [$from, $to])->count();
        $searchMessages = AgentMessage::whereBetween('created_at', [$from, $to])->count();
        $searchTransactions = Transaction::whereBetween('created_at', [$from, $to])->count();
        $searchPayments = Payment::whereBetween('created_at', [$from, $to])->where('is_late', 0)->sum('cost');
    
        return view('details.search', compact(
            'searchUsers', 'searchMessages', 'searchTransactions', 'searchPayments', 'from', 'to'
        ));
    }
    
    public function agentReportShow (Request $request, $id)
    {
    	if (isset($request->month)) {
        	$monthDate = $request->month;
        } else {
        	$monthDate = date('m');
        }
    	
        $Agent = Agent::find($id);
        $Transaction = Transaction::where('agent_id', $id)
        							->whereMonth('created_at', $monthDate)
                                    ->get();
        $successTrans = Transaction::where('is_successful', 1)
                					->whereMonth('created_at', $monthDate)
                                    ->where('agent_id', $id)
                                    ->count();
        $deservedMoney = Transaction::where('is_successful', 1)
        							  ->whereMonth('created_at', $monthDate)
                                      ->where('agent_id', $id)
                                      ->sum('cost');
        
        return view('report.agentReportShow', compact('Agent', 'Transaction', 'monthDate', 'successTrans', 'deservedMoney'));
    }
    
    public function agentReportSearch (Request $request, $id)
    {
    	if (isset($request->month)) {
        	$monthDate = $request->month;
        } else {
        	$monthDate = date('m');
        }
    	
        $Agent = Agent::find($id);
        $Transaction = Transaction::where('agent_id', $id)
        							->whereMonth('created_at', $monthDate)
                                    ->get();
        $successTrans = Transaction::where('is_successful', 1)
                					->whereMonth('created_at', $monthDate)
                                    ->where('agent_id', $id)
                                    ->count();
        $deservedMoney = Transaction::where('is_successful', 1)
        							  ->whereMonth('created_at', $monthDate)
                                      ->where('agent_id', $id)
                                      ->sum('cost');
        
        return view('report.agentReportShow', compact('Agent', 'Transaction', 'monthDate', 'successTrans', 'deservedMoney'));
    }
    
    public function agentInfo ($id)
    {

        $monthDate = date('m');
    	
        $Agent = Agent::find($id);
        $transactionCount = Transaction::where('agent_id', $id)
                                    ->count();
        $deservedMoney = Transaction::where('is_successful', 1)
        							  ->whereMonth('created_at', $monthDate)
                                      ->where('agent_id', $id)
                                      ->sum('cost');
        $allMonthTransactions = Transaction::where('agent_id', $id)
        							->whereMonth('created_at', $monthDate)
                                    ->get();
        
        return view('report.agentReportShow', compact('Agent', 'transactionCount', 'deservedMoney'));
    }
    
    public function agentIndex()
    {
        
        $Date = date('Y-m-d');
        $monthDate = date('m');
        $yearDate = date('Y');
        $dayDate = date('d');

        $Agents = Agent::all();
        $agents = [];
        $oneAgent = [];
        foreach ($Agents as $agent) {
            $oneAgent['id'] = $agent->id;
            $oneAgent['name'] = $agent->first_name . $agent->last_name ;
            $oneAgent['DP'] = Payment::whereDay('created_at', $dayDate)
                                       ->where('is_late', 0)
                                       ->where('agent_id', $agent->id)
                                       ->sum('cost');
            $oneAgent['MP'] = Payment::whereMonth('created_at', $monthDate)
                                       ->where('is_late', 0)
                                       ->where('agent_id', $agent->id)
                                       ->sum('cost');
            $oneAgent['YP'] = Payment::whereYear('created_at', $yearDate)
                                       ->where('is_late', 0)
                                       ->where('agent_id', $agent->id)
                                       ->sum('cost');




            $oneAgent['DUP'] = Payment::whereDay('created_at', $dayDate)
                                       ->where('is_late', 1)
                                       ->where('agent_id', $agent->id)
                                       ->sum('cost');
            $oneAgent['MUP'] = Payment::whereMonth('created_at', $monthDate)
                                       ->where('is_late', 1)
                                       ->where('agent_id', $agent->id)
                                       ->sum('cost');
            $oneAgent['YUP'] = Payment::whereYear('created_at', $yearDate)
                                       ->where('is_late', 1)
                                       ->where('agent_id', $agent->id)
                                       ->sum('cost');

            $agents[] = $oneAgent;
            $oneAgent = [];
        }
        //dd($agents);

        return view('report.agentIndex', compact('agents'));
    }
}
