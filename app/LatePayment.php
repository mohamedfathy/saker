<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LatePayment extends Model
{
    protected $table = 'late_payment';
    public $timestamps = false;
    
    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agent_id', 'id');
    }

}
