<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $table = 'messages';
    public $timestamps = false;

    public function MessageType()
    {
        return $this->belongsTo('App\MessageType', 'message_type_id', 'id');
    }

    public function Language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'id');
    }
}
