<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCompany extends Model
{
	protected $table = 'agents_companies';
    public $timestamps = false;
}
