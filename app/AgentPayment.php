<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentPayment extends Model
{
	protected $table = 'agents_payments';
    public $timestamps = false;
    
    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agent_id', 'id');
    }
}
