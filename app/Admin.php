<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
	protected $table = 'admins';
    public $timestamps = false;

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }
}
