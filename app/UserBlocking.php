<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBlocking extends Model
{
    protected $table = 'user_blocking';
    public $timestamps = false;
}
