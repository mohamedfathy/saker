<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
	protected $table = 'agents';
    public $timestamps = false;

    public function nationality()
    {
        return $this->belongsTo('App\Nationality', 'nationality_id', 'id');
    }
}
