<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentMessage extends Model
{
	protected $table = 'agents_messages';
    public $timestamps = false;

    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agent_id', 'id');
    }
    
    public function message()
    {
        return $this->belongsTo('App\Message', 'message_id', 'id');
    }

    public function messageType()
    {
        return $this->belongsTo('App\MessageType', 'message_type_id', 'id');
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin', 'admin_id', 'id');
    }
}
