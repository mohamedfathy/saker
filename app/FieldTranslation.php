<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldTranslation extends Model
{
	protected $table = 'fields_translation';
    public $timestamps = false;
}
