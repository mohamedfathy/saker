<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $table = 'transactions';
    public $timestamps = false;
    
    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agent_id', 'id');
    }
}
