<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginHistory extends Model
{
	protected $table = 'login_history';
    public $timestamps = false;
    
    public function admin()
    {
        return $this->belongsTo('App\Admin', 'admin_id', 'id');
    }
    
    public function scopeIdDescending($query)
	{
        return $query->orderBy('id','DESC');
	}  
}
