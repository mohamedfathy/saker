<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NationalityTranslation extends Model
{
	protected $table = 'nationalites_translation';
    public $timestamps = false;
}
