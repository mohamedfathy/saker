@extends ('layouts.master')
@section('title', "التفاصيل")
@section ('content')

<div class="row">
    <form action="/details/search" method="POST" dir="rtl">
    @csrf <!-- Handel the Cross Site Request Forgery -->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('from') ? ' has-error' : '' }}">
            <input type="date" name="from" class="form-control" placeholder="من" value="{{ old('from') }}"> @if ($errors->has('from'))
            <span style="color:red;">{{ $errors->first('from') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('to') ? ' has-error' : '' }}">
            <input  type="date" name="to" class="form-control" placeholder="الى" value="{{ old('to') }}"> @if ($errors->has('to'))
            <span style="color:red;">{{ $errors->first('to') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->
    
    <div class="col-md-2">
		<button type="submit" class="form-control btn btn-sm btn-primary" > بحث </button>
    </div>
    <!--end col-md-2-->
</div>
<!--row-->

<h2 style="text-align:right;">العملاء</h2>
<!-- begin row -->
<div class="row">
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
        <h4>اجمالى العملاء المسجلين اليوم</h4>
        <p>{{$dailyUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
      <h4>اجمالى العملاء المسجلين اسبوعى</h4>
      <p>{{$weeklyUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
      <h4>اجمالى العملاء المسجلين شهريا</h4>
      <p>{{$monthlyUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
      <h4>اجمالى العملاء المسجلين سنويا</h4>
      <p>{{$yearlyUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->



<h2 style="text-align:right;">الرسائل</h2>
<!-- begin row -->
<div class="row">
 
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
        <h4>اجمالى الرسائل اليومية</h4>
        <p>{{$dailyMessages}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
      <h4>اجمالى الرسائل الاسبوعية</h4>
      <p>{{$weeklyMessages}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
      <h4>اجمالى الرسائل الشهرية</h4>
      <p>{{$monthlyMessages}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
      <h4>اجمالى الرسائل السنوية</h4>
      <p>{{$yearlyMessages}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->


<h2 style="text-align:right;">المعاملات</h2>
<!-- begin row -->
<div class="row">
 
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
        <h4>اجمالى المعاملات اليومية</h4>
        <p>{{$dailyTransactions}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
      <h4>اجمالى المعاملات الاسبوعية</h4>
      <p>{{$weeklyTransactions}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
      <h4>اجمالى المعاملات الشهرية</h4>
      <p>{{$monthlyTransactions}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
      <h4>اجمالى المعاملات السنوية</h4>
      <p>{{$yearlyTransactions}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->


<h2 style="text-align:right;">النفقات</h2>
<!-- begin row -->
<div class="row">
 
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
        <h4>اجمالى النفقات يومية</h4>
        <p>{{$dailyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات الاسبوعية</h4>
      <p>{{$weeklyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات شهرية</h4>
      <p>{{$monthlyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات سنوية</h4>
      <p>{{$yearlyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->
@endsection