@extends ('layouts.master')
@section('title', "التفاصيل")
@section ('content')

<div class="row">
    <form action="/details/search" method="POST" dir="rtl">
    @csrf <!-- Handel the Cross Site Request Forgery -->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('from') ? ' has-error' : '' }}">
            <input type="date" name="from" class="form-control" placeholder="من" value="{{$from}}"> @if ($errors->has('from'))
            <span style="color:red;">{{ $errors->first('from') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('to') ? ' has-error' : '' }}">
            <input  type="date" name="to" class="form-control" placeholder="الى" value="{{$to}}"> @if ($errors->has('to'))
            <span style="color:red;">{{ $errors->first('to') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->
    
    <div class="col-md-2">
		<button type="submit" class="form-control btn btn-sm btn-primary" > بحث </button>
    </div>
    <!--end col-md-2-->
</div>
<!--row-->

<h2 style="text-align:right;">نتائج البحث</h2>
<!-- begin row -->
<div class="row">
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
        <h4> العملاء </h4>
        <p>{{$searchUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->

  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
        <h4> الرسائل </h4>
        <p>{{$searchMessages}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->

  <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
        <h4> المعاملات </h4>
        <p>{{$searchTransactions}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->

  <!-- begin col-3 -->
    <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
        <h4> النفقات </h4>
        <p>{{$searchPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
</div><!-- end row -->
@endsection