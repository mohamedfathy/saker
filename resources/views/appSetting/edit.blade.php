@extends ('layouts.master')
@section('title', "تعديل اعدادات التطبيق")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div><!-- panel-heading-btn -->
        <h4 class="panel-title">تعديل اعدادات التطبيق  </h4>
      </div><!--panel-heading -->

      <div class="panel-body">
        <form action="/appSetting/{{$AppSetting->id}}" method="POST" dir="rtl">
          @method('PUT') <!-- Spoofing Form Methods -->
          @csrf          <!-- Handel the Cross Site Request Forgery -->
          <input type="hidden" name="id" value="{{$AppSetting->id}}">

        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('comission_per_transaction') ? ' has-error' : '' }}">
                <label for="comission_per_transaction">العمولة لكل معاملة</label>
                <input type="text" name="comission_per_transaction" class="form-control"  value="{{$AppSetting->comission_per_transaction}}">
                @if ($errors->has('comission_per_transaction'))
                <span style="color:red;">{{ $errors->first('comission_per_transaction') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->


            <div class="col-md-6">
              <div class="form-group {{ $errors->has('payment_alerting_day') ? ' has-error' : '' }}">
                <label for="payment_alerting_day">يوم تنبيه الدفع</label>
                <input type="text" name="payment_alerting_day" class="form-control"  value="{{$AppSetting->payment_alerting_day}}">
                @if ($errors->has('payment_alerting_day'))
                <span style="color:red;">{{ $errors->first('payment_alerting_day') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
        </div><!--row-->


        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('allowed_payment_period') ? ' has-error' : '' }}">
                <label for="allowed_payment_period"> فترة سماح السداد</label>
                <input type="text" name="allowed_payment_period" class="form-control"  value="{{$AppSetting->allowed_payment_period}}">
                @if ($errors->has('allowed_payment_period'))
                <span style="color:red;">{{ $errors->first('allowed_payment_period') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->

        </div><!--row-->




        <div class="row">
        <div class="col-md-12">
            <hr />
            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> حفظ </button>
        </div><!--end col-md-12-->
        </div><!--row-->

        </form>
      </div><!--panel-body-->

    </div><!--panel panel-inverse-->
  </div><!--col-md-12-->
</div><!--row-->
@endsection