@extends ('layouts.master')
@section('title', "سجل دخول المستخدمين")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">سجل دخول المستخدمين</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>#</th>
              <th>عنوان بروتوكول الإنترنت</th>
              <th>الجهاز</th>
              <th>المدير</th>
              <th>تاريخ التسجيل</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($Log as $L)
            <tr>
              <td>{{ $L->id }}</td>
              <td>{{ $L->ip }}</td>
              <td>{{ $L->device }}</td>
              <td>{{ $L->admin->first_name }} {{ $L->admin->second_name }} {{ $L->admin->third_name }}</td>
              <td>{{ $L->created_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection