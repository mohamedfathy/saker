@extends ('layouts.master')
@section('title', "اعدادات التطبيق")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title"> اعدادات التطبيق </h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th> العمولة لكل معاملة</th>
              <th>يوم تنبيه الدفع </th>
              <th> فترة سماح السداد</th>
              <th>تعديل</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($AppSetting as $AS)
            <tr>
              <td>{{ $AS->comission_per_transaction }}</td>
              <td>{{ $AS->payment_alerting_day }}</td>
              <td>{{ $AS->allowed_payment_period}}</td>
              <td><a href="/appSetting/{{$AS->id}}/edit" class="btn btn-success btn-xs">تعديل</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">استيراد و تصدير قاعدة البيانات </h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table class="table table-bordered" dir="rtl" >
          <tbody>
            <tr>
              <td>
              استيراد
              </td>
              <td>
              <form action="/appSetting/import" method="POST"  enctype="multipart/form-data">
                @csrf <!-- Handel the Cross Site Request Forgery -->
                  <input type="file" name="sql_url" style="width: 250px;margin: 0 auto;"><br />
                  <input type="submit" name="import" class="btn btn-success btn-md btn-block" value="استيراد" />
                </div><!--col-md-6-->
              </form>
              </td>
            </tr>
            


                        <tr>
              <td>
              تصدير 
              </td>
              <td>
                <a href="/appSetting/export" class="btn btn-success btn-md btn-block">تصدير</a>
              </td>
            </tr>
            
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection