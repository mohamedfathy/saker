@extends ('layouts.master')
@section('title', "المستخدمين المحجوبين")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">المستخدمين المحجوبين</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <h5 class="page-header"><a href="/blockedUsers/create" class="btn btn-primary btn-xs">اضافة حجب</a></h5><!-- page-header --><hr />

        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>#</th>
              <th>عنوان بروتوكول الإنترنت</th>
              <th>تعديل</th>
              <th>حذف</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($BlockedUser as $BU)
            <tr>
              <td>{{ $BU->id }}</td>
              <td>{{ $BU->ip }}</td>
              <td><a href="/blockedUsers/{{$BU->id}}/edit" class="btn btn-success btn-xs">تعديل</a></td>
              <td><a href="/blockedUsers/{{$BU->id}}/destroy" class="btn btn-danger btn-xs" onclick="return confirm('سيتم حذف البيانات نهائيا ... هل انت متاكد؟');">حذف</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection