@extends ('layouts.master')
@section('title', "المدير")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div><!-- panel-heading-btn -->
        <h4 class="panel-title">المدير</h4>
      </div>

      <div class="panel-body">
        <div class="profile-section">

          <div class="profile-left">
            <div class="profile-image" style="height:180px;background-image: url('{{ asset((($Admin->photo_url == NULL) ? '/uploads/agents/agent.png' : $Admin->photo_url) ) }}'); background-repeat: no-repeat;background-size: cover;">
                            <i class="fa fa-user hide"></i>
            </div><!-- profile-image -->
          </div><!-- profile-left -->

          <div class="profile-right">
            <div class="profile-info">
              <div class="table-responsive">
                <table class="table table-profile">
                  <thead>
                    <tr>
                      <th colspan="2">
                        <h4>{{$Admin->first_name}} {{$Admin->second_name}} {{$Admin->third_name}}<small>{{$Admin->type}}</small></h4>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="highlight">
                      <td class="field">الاسم</td>
                      <td>{{$Admin->first_name}} {{$Admin->second_name}} {{$Admin->third_name}}</td>
                    </tr>
                    <tr class="divider">
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td class="field">الرقم القومى</td>
                      <td><i class="fa fa-mobile fa-lg m-r-5"></i> {{$Admin->national_number}}</td>
                    </tr>
                    <tr>
                    <tr>
                      <td class="field">الايميل</td>
                      <td>{{$Admin->email}}</td>
                    </tr>
                    <tr class="divider">
                      <td colspan="2"></td>
                    </tr>

                    <tr class="divider">
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td class="field">الحالة</td>
                      <td>
                      @if ($Admin->is_active === 0) {{'غير نشط'}} 
                      @else {{'نشط'}}
                      @endif
                      </td>
                    </tr>
                    <tr>
                        <td class="field">الدور</td>
                        <td>
                          {{$Admin->role->name}}
                        </td>
                      </tr>
                    <tr>
                      <td class="field"> التسجيل</td>
                      <td>{{Carbon\Carbon::parse($Admin->created_at)->toDayDateTimeString()}}</td>
                    </tr>                   
                  </tbody>
                </table>
              </div><!-- table-responsive -->
            </div><!-- profile-info -->
          </div><!-- profile-right -->
        </div><!--profile-section -->
      </div><!--panel-body -->

    </div><!-- panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- row -->
@endsection