@extends ('layouts.master')
@section('title', "انواع الرسائل")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">انواع الرسائل</h4>
      </div><!--panel-heading-->

      <div class="panel-body">

        <h5 class="page-header"><a href="/messagType/create" class="btn btn-primary btn-xs">اضافة نوع رسالة</a></h5><!-- page-header --><hr />
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th># </th>
              <th>نوع الرسالة</th>
              <th>تعديل</th>
              <th>حذف</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($MessageType as $MT)
            <tr>
              <td>{{ $MT->id }}</td>
              <td>{{ $MT->name }}</td>
              <td><a href="/messagType/{{$MT->id}}/edit" class="btn btn-success btn-xs">تعديل</a></td>
              <td><a href="/messagType/{{$MT->id}}/destroy" class="btn btn-danger btn-xs" onclick="return confirm('سيتم حذف البيانات نهائيا ... هل انت متاكد؟');">حذف</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection