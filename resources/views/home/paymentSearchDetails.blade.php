@extends ('layouts.master')
@section('title', "تفاصيل النفقات")
@section ('content')

<div class="row">
  <form action="/home/paymentsDetails/search" method="POST" dir="rtl">
    @csrf <!-- Handel the Cross Site Request Forgery -->
    
    <div class="col-md-5">
        <div class="form-group {{ $errors->has('from') ? ' has-error' : '' }}">
            <label for="from">من</label>
            <input type="date" name="from" class="form-control" placeholder="من" value="{{$from}}"> @if ($errors->has('from'))
            <span style="color:red;">{{ $errors->first('from') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('to') ? ' has-error' : '' }}">
            <label for="to">الى</label>
            <input  type="date" name="to" class="form-control" placeholder="الى" value="{{$to}}"> @if ($errors->has('to'))
            <span style="color:red;">{{ $errors->first('to') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->
    
    <div class="col-md-2">
        <label for="submit">بحث</label>
        <button type="submit" class="form-control btn btn-sm btn-primary" > بحث </button>
    </div>
    <!--end col-md-2-->
  </form>
</div>
<!--row-->
<br />
<div class="row">
  <!-- begin col-3 -->
    <div class="col-md-12 ">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
        <h4> اجمالى النفقات </h4>
        <p>{{$SearchPaymentsCost}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
</div>
<!--row-->


<h2 style="text-align:right;">تفاصيل النفقات</h2>
<!-- begin row -->
<div class="row">
<div class="col-md-12">
  <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">نتائج بحث النفقات</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>التكلفة</th>
              <th>الحالة</th>
              <th>متاخر</th>
              <th>اسم العميل</th>
              <th>تاريخ الانشاء</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($SearchPayments as $SP)
              <tr>
              <td>{{ $SP->cost }}</td>
              <td>{{ $SP->status }}</td>
              <td style="text-align:center;">
                @if ($SP->is_late == 0)
                  <a href="/agents/{{$SP->id}}/activation">
                  <input type="checkbox" data-render="switchery" data-theme="default" checked /> متاخر
                  </a>
                @else
                  <a href="/agents/{{$SP->id}}/activation">
                  <input type="checkbox" data-render="switchery" data-theme="default" />غير متاخر
                  </a>
                @endif
              </td>
              <td>{{ $SP->agent_id }}</td>
              <td>{{ $SP->created_at }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection