@extends ('layouts.master')
@section('title', "تفاصيل النفقات")
@section ('content')

<div class="row">
  <form action="/home/paymentsDetails/search" method="POST" dir="rtl">
    @csrf <!-- Handel the Cross Site Request Forgery -->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('from') ? ' has-error' : '' }}">
            <label for="from">من</label>
            <input type="date" name="from" class="form-control"> @if ($errors->has('from'))
            <span style="color:red;">{{ $errors->first('from') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('to') ? ' has-error' : '' }}">
            <label for="to">الى</label>
            <input  type="date" name="to" class="form-control"> @if ($errors->has('to'))
            <span style="color:red;">{{ $errors->first('to') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->
    
    <div class="col-md-2">
      <label for="submit">بحث</label>
		  <button type="submit" name="submit" class="form-control btn btn-sm btn-primary" > بحث </button>
    </div>
    <!--end col-md-2-->
  </form>
</div>
<!--row-->

<h2 style="text-align:right;">النفقات</h2>
<!-- begin row -->
<div class="row">
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
        <h4>اجمالى النفقات يومية</h4>
        <p>{{$dailyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات الاسبوعية</h4>
      <p>{{$weeklyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات شهرية</h4>
      <p>{{$monthlyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات سنوية</h4>
      <p>{{$yearlyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->
@endsection