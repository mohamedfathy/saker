@extends ('layouts.master')
@section('title', "تفاصيل العملاء")
@section ('content')

<div class="row">
  <form action="/home/agentsDetails/search" method="POST" dir="rtl">
    @csrf <!-- Handel the Cross Site Request Forgery -->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('from') ? ' has-error' : '' }}">
            <label for="from">من</label>
            <input type="date" name="from" class="form-control"> @if ($errors->has('from'))
            <span style="color:red;">{{ $errors->first('from') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->

    <div class="col-md-5">
        <div class="form-group {{ $errors->has('to') ? ' has-error' : '' }}">
            <label for="to">الى</label>
            <input  type="date" name="to" class="form-control"> @if ($errors->has('to'))
            <span style="color:red;">{{ $errors->first('to') }}</span>
            @endif
        </div>
        <!--end form-group-->
    </div>
    <!--end col-md-5-->
    
    <div class="col-md-2">
      <label for="submit">بحث</label>
		  <button type="submit" name="submit" class="form-control btn btn-sm btn-primary" > بحث </button>
    </div>
    <!--end col-md-2-->
  </form>
</div>
<!--row-->

<h2 style="text-align:right;">العملاء</h2>
<!-- begin row -->
<div class="row">
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
        <h4>اجمالى العملاء المسجلين اليوم</h4>
        <p>{{$dailyUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
      <h4>اجمالى العملاء المسجلين اسبوعى</h4>
      <p>{{$weeklyUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
      <h4>اجمالى العملاء المسجلين شهريا</h4>
      <p>{{$monthlyUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
      <h4>اجمالى العملاء المسجلين سنويا</h4>
      <p>{{$yearlyUsers}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->
@endsection