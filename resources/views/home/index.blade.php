@extends ('layouts.master')
@section('title', "الرئيسية")
@section ('content')

<!-- begin row -->
<div class="row">
{!! Charts::styles() !!}
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-users"></i></div>
      <div class="stats-info">
        <h4>اجمالى العملاء</h4>
        <p>{{$AgentsCount}}</p>  
      </div>
      <div class="stats-link">
        <a href="/home/agentsDetails"><i class="fa fa-arrow-circle-o-left"></i> عرض التفاصيل</a>
      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
      <h4>اجمالى الرسائل</h4>
      <p>{{$MessagesCount}}</p>  
      </div>
      <div class="stats-link">
        <a href="/home/messagesDetails"><i class="fa fa-arrow-circle-o-left"></i> عرض التفاصيل </a>
      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
      <h4>اجمالى المعاملات</h4>
      <p>{{$TransactionsCount}}</p>  
      </div>
      <div class="stats-link">
        <a href="/home/transactionsDetails"><i class="fa fa-arrow-circle-o-left"></i> عرض التفاصيل</a>
      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-3 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات</h4>
      <p>{{$PaymentsTotal}}</p>
      </div>
      <div class="stats-link">
        <a href="/home/paymentsDetails"><i class="fa fa-arrow-circle-o-left"></i> عرض التفاصيل </a>
      </div>
    </div>
  </div>
  <!-- end col-3 -->
</div><!-- end row -->


<div class="row">
  <div class="col-md-6 col-sm-12">
  <div class="panel panel-inverse" data-sortable-id="index-1">
      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div>
          <h4 class="panel-title">العملاء</h4>
      </div>
  	<div class="panel-body">
      {!! $AgentChart->html() !!}
  	</div>
  </div>
  </div><!-- end col-md-6 col-sm-12 -->

  <div class="col-md-6 col-sm-12">
  <div class="panel panel-inverse" data-sortable-id="index-1">
      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div>
          <h4 class="panel-title">المعاملات</h4>
      </div>
    <div class="panel-body">
      {!! $TransactionChart->html() !!}
    </div>
  </div>
  </div><!-- end col-md-6 col-sm-12 -->
</div><!-- end row -->

<div class="row">
  <div class="col-md-6 col-sm-12">
  <div class="panel panel-inverse" data-sortable-id="index-1">
      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div>
          <h4 class="panel-title">الرسائل</h4>
      </div>
    <div class="panel-body">
      {!! $MessageChart->html() !!}
    </div>
  </div>
  </div><!-- end col-md-6 col-sm-12 -->

  <div class="col-md-6 col-sm-12">
    <div class="panel panel-inverse" data-sortable-id="index-1">
      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div>
          <h4 class="panel-title">النفقات</h4>
      </div>
      <div class="panel-body">
          {!! $CostChart->html() !!}
      </div>
    </div>
  </div><!-- end col-md-6 col-sm-12 -->
</div><!-- end row -->


    {!! Charts::scripts() !!}
    {!! $AgentChart->script() !!}
    {!! $TransactionChart->script() !!}
    {!! $MessageChart->script() !!}
    {!! $CostChart->script() !!}
@endsection