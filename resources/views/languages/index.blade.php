@extends ('layouts.master')
@section('title', "اللغات")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">اللغات</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <h5 class="page-header"><a href="/languages/create" class="btn btn-primary btn-xs">اضافة لغة</a></h5><!-- page-header --><hr />
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>#</th>
              <th>اللغة</th>
              <th>حذف</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($Language as $L)
            <tr>
              <td>{{ $L->id }}</td>
              <td>{{ $L->name }}</td>
              <td><a href="/languages/{{$L->id}}/destroy" class="btn btn-danger btn-xs" onclick="return confirm('سيتم حذف البيانات نهائيا ... هل انت متاكد؟');">حذف</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection