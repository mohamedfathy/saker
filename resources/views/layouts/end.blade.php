<!-- ================== BEGIN BASE JS ================== -->
<script src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
  <script src="/assets/crossbrowserjs/html5shiv.js"></script>
  <script src="/assets/crossbrowserjs/respond.min.js"></script>
  <script src="/assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>


<!-- ================== END BASE JS ================== -->

  <!-- ================== BEGIN PAGE LEVEL JS ================== -->
  <script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
  <script src="/assets/plugins/flot/jquery.flot.min.js"></script>
  <script src="/assets/plugins/flot/jquery.flot.time.min.js"></script>
  <script src="/assets/plugins/flot/jquery.flot.resize.min.js"></script>
  <script src="/assets/plugins/flot/jquery.flot.pie.min.js"></script>
  <script src="/assets/plugins/sparkline/jquery.sparkline.js"></script>
  <!-- <script src="/assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script> -->
  <!-- <script src="/assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script> -->
  <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <!-- ================== END PAGE LEVEL JS ================== -->

  <!-- ================== BEGIN Data TAbles JS ================== -->
  <script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
  <script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
  <script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
  <script src="/assets/js/table-manage-buttons.demo.min.js"></script>
  <script src="/assets/js/apps.min.js"></script>
  <script src="/assets/plugins/masked-input/masked-input.min.js"></script>
  
  <!-- ================== BEGIN Data TAbles JS ================== -->
  <script src="/assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
  <script src="/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
  <script src="/assets/plugins/masked-input/masked-input.min.js"></script>
  <script src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="/assets/plugins/password-indicator/js/password-indicator.js"></script>
  <script src="/assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js"></script>
  <script src="/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
  <script src="/assets/plugins/jquery-tag-it/js/tag-it.min.js"></script>
  <script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
  <script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

  <script src="/assets/js/form-plugins.demo.min.js"></script>
  <script src="/assets/js/apps.min.js"></script>
  <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
  <script src="/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
  <script src="/assets/plugins/masked-input/masked-input.min.js"></script>
  <script src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
  <!-- ================== END Data Tables JS ================== -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
  <script src="/assets/js/dashboard.min.js"></script>
  <script src="/assets/js/apps.min.js"></script>
  <script src="/assets/js/table-manage-responsive.demo.min.js"></script>
  <script src="/assets/plugins/switchery/switchery.min.js"></script>
  <script src="/assets/js/form-slider-switcher.demo.min.js"></script>
  
  
<script>

jQuery(function($){
   $("#identty").mask("999-9999-9999999-9");
});

$('#datepicker-default').datepicker({
    format: 'mm/dd/yyyy'
});




$('#datepicker-default1').datepicker({
    format: 'mm/dd/yyyy'
});

</script>
  <script>
    $(document).ready(function() {
      App.init();
      Dashboard.init();
      TableManageButtons.init();
      FormSliderSwitcher.init();
      FormPlugins.init();
    });
  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53034621-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
