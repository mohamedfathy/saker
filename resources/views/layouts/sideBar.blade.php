<!-- begin #sidebar -->
<div id="sidebar" class="sidebar sidebar-transparent">
  <!-- begin sidebar scrollbar -->
  <div data-scrollbar="true" data-height="100%">
    <!-- begin sidebar user -->
    <ul class="nav">
      <li class="nav-profile">
        <div class="image">
          <a href="javascript:;">
          <img src="/assets/img/user-13.jpg" alt="" /></a>
        </div>
        <div class="info">
          لوحة التحكم
          <small>الصقر</small>
        </div>
      </li>
    </ul>
    <!-- end sidebar user -->
    <!-- begin sidebar nav -->
    <ul class="nav">

      <li class="{{ Request::is('home*') ? 'active ' : '' }}">
        <a href="/home">
            <i class="fa fa-dashboard"></i>
            <span>الرئيسية</span>
        </a>
      </li>

      <li class="{{ Request::is('admins*') ? 'active ' : '' }}">
        <a href="/admins">
            <i class="fa fa-user"></i>
            <span>المستخدمين</span>
        </a>
      </li>



    <li class="has-sub {{ 
      Request::is('agents*') || 
      Request::is('agentPayments*') ||
     
      Request::is('agentReport*')
      ? 'active ' : '' }}">
      <a href="javascript:;">
          <b class="caret pull-left"></b>
          <i class="fa fa-users"></i>
          <span>مندوبي الشركات</span>
        </a>
      <ul class="sub-menu">
          <li class="{{ Request::is(Route::current()->getName() == 'agents') ? 'active' : '' }}">
          <a href="/agents">مندوبي الشركات</a>
          </li>
          <li class="{{ Request::is(Route::current()->getName() == 'agentPayments') ? 'active' : '' }}">
          <a href="/agentPayments">مستحقات مندوب شركة</a>
          </li>
          <li class="{{ Request::is(Route::current()->getName() == 'agentReport') ? 'active' : '' }}">
          <a href="/agentReport">تقرير مندوب شركة</a>
          </li>
      </ul>
    </li>


    <li class="has-sub {{ 
      Request::is('message*') || 
      Request::is('agentMessages*') ||
      Request::is('messagType*') 
      ? 'active ' : '' }}">
      <a href="javascript:;">
          <b class="caret pull-left"></b>
          <i class="fa fa-rocket"></i>
          <span>الرسائل</span>
        </a>
      <ul class="sub-menu">
          <li class="{{ Request::is(Route::current()->getName() == 'message') ? 'active' : '' }}">
          <a href="/message">نماذج الرسائل</a>
          </li>
          <li class="{{ Request::is(Route::current()->getName() == 'messagType') ? 'active' : '' }}">
          <a href="/messagType">انواع الرسائل</a>
          </li>
          <li class="{{ Request::is(Route::current()->getName() == 'agentMessages') ? 'active' : '' }}">
          <a href="/agentMessages">التقارير و الارسال </a>
          </li>
      </ul>
    </li>

    <li class="{{ Request::is('transactions/create') ? 'active ' : '' }}">
      <a href="/transactions/create">
          <i class="fa fa-plus-circle"></i>
          <span>تسجيل معاملة </span>
      </a>
    </li>
    
    <li class="{{ Request::is('transactions*') && !Request::is('transactions/create')  ? 'active ' : '' }}">
      <a href="/transactions">
          <i class="fa fa-exchange"></i>
          <span>المعاملات</span>
      </a>
    </li>
      

      <li class="{{ Request::is('nationalities*') ? 'active ' : '' }}">
        <a href="/nationalities">
            <i class="fa fa-flag"></i>
            <span>الجنسيات</span>
        </a>
      </li>


      <li class="{{ Request::is('blockedUsers*') ? 'active ' : '' }}">
        <a href="/blockedUsers">
            <i class="fa fa-ban"></i>
            <span>حجب جهاز</span>
        </a>
      </li>          
          
      <li class="{{ Request::is('companies*') ? 'active ' : '' }}">
        <a href="/companies">
            <i class="fa fa-building-o"></i>
            <span>المنشآت</span>
        </a>
      </li>



      
      <li class="{{ Request::is('IndexLog*') ? 'active ' : '' }}">
        <a href="/IndexLog">
            <i class="fa fa-sign-in"></i>
            <span>سجلات التسجيل</span>
        </a>
      </li>
      
      <li class="{{ Request::is('languages*') ? 'active ' : '' }}">
        <a href="/languages">
            <i class="fa fa-language"></i>
            <span>اللغات</span>
        </a>
      </li>

      <li class="{{ Request::is('report*') ? 'active ' : '' }}">
        <a href="/report">
            <i class="fa fa-file"></i>
            <span>التقارير</span>
        </a>
      </li>

      <li class="{{ Request::is('latePayments*') ? 'active ' : '' }}">
        <a href="/latePayments">
            <i class="fa fa-file"></i>
            <span>مدفوعات متاخرة</span>
        </a>
      </li>

      
      <li class="{{ Request::is('appSetting*') ? 'active ' : '' }}">
        <a href="/appSetting">
            <i class="fa fa-cog"></i>
            <span>اعدادات التطبيق</span>
        </a>
      </li>
      
      <li class="{{ Request::is('complains*') ? 'active ' : '' }}">
        <a href="/complains">
            <i class="fa fa-commenting-o"></i>
            <span>الشكاوى و الاقتراحات</span>
        </a>
      </li>
      
     <li class="{{ Request::is('agentPage*') ? 'active ' : '' }}">
        <a href="/agentPage">
            <i class="fa fa-users"></i>
            <span>صفحة المندوبين </span>
        </a>
      </li>



          <!-- begin sidebar minify button -->
      <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
          <!-- end sidebar minify button -->
    </ul>
    <!-- end sidebar nav -->
  </div>
  <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar-->