@extends ('layouts.master')
@section('title', "المنشآت")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">المنشآت</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
          @guest
          @else 
          @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
        <h5 class="page-header"><a href="/companies/create" class="btn btn-primary btn-xs">اضافة منشآة</a></h5><!-- page-header --><hr />
        @endif 
        @endguest
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>الاسم عربى</th>
              <th>الاسم انجليزى</th>
              <th>رقم التليفون</th>
              <th>الرخصة</th>
              <th>رقم المنشآة</th>
              @guest
              @else 
              @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
              <th>تعديل</th>
              <th>حذف</th>
              @endif 
              @endguest
            </tr>
          </thead>
          <tbody>
            @foreach ($company as $C)
            <tr>
              <td>{{ $C->name_ar }}</td>
              <td>{{ $C->name_en }}</td>
              <td>{{ $C->number }}</td>
              <td>{{ $C->license }}</td>
              <td>{{ $C->company_number }}</td>
              @guest
              @else 
              @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
              <td><a href="/companies/{{$C->id}}/edit" class="btn btn-success btn-xs">تعديل</a></td>
              <td><a href="/companies/{{$C->id}}/destroy" class="btn btn-danger btn-xs" onclick="return confirm('سيتم حذف البيانات نهائيا ... هل انت متاكد؟');">حذف</a></td>
              @endif 
              @endguest
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection