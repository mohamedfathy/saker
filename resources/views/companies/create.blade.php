@extends ('layouts.master')
@section('title', "اضافة منشأة")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div><!-- panel-heading-btn -->
        <h4 class="panel-title"> اضافة منشأة </h4>
      </div><!--panel-heading -->

      <div class="panel-body">
        <form action="/companies" method="POST" dir="rtl" enctype="multipart/form-data">
          @csrf <!-- Handel the Cross Site Request Forgery -->

        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('name_ar') ? ' has-error' : '' }}">
                <label for="name_ar">الاسم عربى</label>
                <input type="text" name="name_ar" class="form-control"  value="{{ old('name_ar') }}">
                @if ($errors->has('name_ar'))
                <span style="color:red;">{{ $errors->first('name_ar') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->

            <div class="col-md-6">
              <div class="form-group {{ $errors->has('name_en') ? ' has-error' : '' }}">
                <label for="name_en">الاسم انجليزى</label>
                <input type="text" name="name_en" class="form-control"  value="{{ old('name_en') }}">
                @if ($errors->has('name_en'))
                <span style="color:red;">{{ $errors->first('name_en') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
        </div><!--row-->
        
        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('company_number') ? ' has-error' : '' }}">
                <label for="company_number">رقم المنشأة</label>
                <input type="text" name="company_number" class="form-control"  value="{{ old('company_number') }}">
                @if ($errors->has('company_number'))
                <span style="color:red;">{{ $errors->first('company_number') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
            
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('license') ? ' has-error' : '' }}">
                <label for="license">الرخصة</label>
                <input type="text" name="license" class="form-control"  value="{{ old('license') }}">
                @if ($errors->has('license'))
                <span style="color:red;">{{ $errors->first('license') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
        </div><!--row-->

        <div class="row">



            <div class="col-md-6">
              <div class="form-group {{ $errors->has('number') ? ' has-error' : '' }}">
                <label for="number">التليفون</label>
                <input type="text" name="number" class="form-control"  value="{{ old('number') }}">
                @if ($errors->has('number'))
                <span style="color:red;">{{ $errors->first('number') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->

        </div><!--row-->

        <div class="row">
        <div class="col-md-12">
            <hr />
            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> حفظ </button>
        </div><!--end col-md-12-->
        </div><!--row-->

        </form>
      </div><!--panel-body-->

    </div><!--panel panel-inverse-->
  </div><!--col-md-12-->
</div><!--row-->
@endsection