@extends('layouts.login')

@section('content')
<!-- begin login -->
<div class="login login-with-news-feed">
    <!-- begin news-feed -->
    <div class="news-feed" style="direction:rtl;">
        <div class="news-image">
            <img src="assets/img/login-bg/bg-2.jpg" data-id="login-cover-image" alt="" />
        </div>
        <div class="news-caption">
            <h4 class="caption-title"><i class="fa fa-diamond text-success"></i>السقار </h4>
            <p>صفحة كود المندوب</p>
        </div>
    </div>
    <!-- end news-feed -->
    <!-- begin right-content -->
    <div class="right-content" style="direction:rtl;">
        <!-- begin login-header -->
        <div class="login-header">
            <div class="brand">
                <span class="logo"></span> السقار
                <small>صفحة كود المندوب</small>
            </div>
            <div class="icon">
                <i class="fa fa-sign-in"></i>
            </div>
        </div>
        <!-- end login-header -->
        <!-- begin login-content -->
        <div class="login-content">
            <form action="/agentCode" method="POST" class="margin-bottom-0">
            	@csrf
                <div class="form-group m-b-15">
                	<label for="code">كود المندوب</label>
                    <input id="code" type="code" class="form-control input-lg{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('phone') }}" required autofocus />

                @if ($errors->has('code'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('code') }}</strong>
                    </span>
                @endif
                </div>
                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">ادخل الكود </button>
                </div>
                <hr />
                <p class="text-center">
                    السقار  &copy; <?php echo date('Y'); ?>
                </p>
            </form>
        </div>
        <!-- end login-content -->
    </div>
    <!-- end right-container -->
</div>
<!-- end login -->
@endsection