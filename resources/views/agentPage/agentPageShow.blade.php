@extends ('layouts.nosidebar')
@section('title', "تقرير عميل شهرى")
@section ('content')

<div class="row">
<div class="col-md-12">
	<h1 class="text-center">صفحة بيانات مندوب</h1>
</div><!--end col-md-12-->
</div><!--row-->
<div class="row">
<hr />
</div><!--row-->
<div class="row">
    <div class="col-md-4">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>اسم العميل : </strong> {{$Agent->first_name}} {{$Agent->last_name}} {{$Agent->thired_name}}
    </div>
    </div>
    <!--end col-md-4-->
    
    <div class="col-md-4">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>تاريخ الانضمام : </strong> {{$joinDate}}
    </div>
    </div>
    <!--end col-md-4-->
    
    <div class="col-md-4">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>عدد المعاملات منذ بداية التسجيل : </strong> {{$TransactionCount}}
    </div>
    </div>
    <!--end col-md-4-->
</div>
<!--row-->  
<div class="row">

    <div class="col-md-4">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>عدد المعاملات هذا الشهر : </strong> {{$successTrans}}
    </div>
    </div>
    <!--end col-md-4-->
    
    <div class="col-md-4">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>الشهر : </strong> {{$monthDate}}
    </div>
    </div>
    <!--end col-md-4-->
    
    <div class="col-md-4">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>المبلغ المستحق : </strong> {{$deservedMoney}}
    </div>
    </div>
    <!--end col-md-4-->
</div>
<!--row-->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">تقرير عميل شهرى</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>رقم المعاملة</th>
              <th>تاريخ المعاملة</th>
              <th>تمت العملية بنجاح </th>
              <th>التكلفة</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($Transaction as $T)
              <tr>
              <td>{{ $T->number }}</td>
              <td>{{ $T->created_at }}</td>
              <td>{{ $T->is_successful }}</td>
              <td>{{ $T->cost }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->

<div class="row">
	<div class="col-md-12 ">
    	<div class="pull-right">
		    <a href="/complains/create/{{$Agent->id}}">قيم مستوى الخدمة</a> | 
        <a href="#">الاستبيانات</a> |
        <a href="#">الشكاوى و الاقتراحات</a> |
        <a href="/">صفحة  البداية </a> 
        </div>
	</div><!-- col-md-12 -->
</div><!--row-->
@endsection