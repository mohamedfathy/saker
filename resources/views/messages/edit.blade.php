@extends ('layouts.master')
@section('title', "تعديل رسالة")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div><!-- panel-heading-btn -->
        <h4 class="panel-title"> تعديل رسالة</h4>
      </div><!--panel-heading -->

      <div class="panel-body">
        <form action="/message/{{$Message->id}}" method="POST" dir="rtl" enctype="multipart/form-data">
          @csrf <!-- Handel the Cross Site Request Forgery -->
			@method('PUT') <!-- Spoofing Form Methods -->
            
            <input type="hidden" name="id" value="{{$Message->id}}">
        <div class="row">

            <div class="col-md-6">
              <div class="form-group {{ $errors->has('message_type_id') ? ' has-error' : '' }}">
                <label for="message_type_id">نوع الرسالة</label>
                <select class="form-control" name="message_type_id">
                  <option disabled selected value>...</option>
				  @foreach ($MessageType as $MT) 
                  <option value="{{$MT->id}}" @if ( $Message->message_type_id === $MT->id) {{'selected'}} @endif>{{$MT->name}}</option>
                  @endforeach
                </select>
                @if ($errors->has('message_type_id'))
                <span style="color:red;">{{ $errors->first('message_type_id') }}</span>
                @endif
              </div>
            </div><!--end col-md-6-->

            <div class="col-md-6">
              <div class="form-group {{ $errors->has('language_id') ? ' has-error' : '' }}">
                <label for="language_id">اللغة</label>
                <select class="form-control" name="language_id">
                  <option disabled selected value>...</option>
                  @foreach ($Language as $L) 
                  <option value="{{$L->id}}" @if ( $Message->language_id === $L->id) {{'selected'}} @endif>{{$L->name}}</option>
                  @endforeach
                </select>
                @if ($errors->has('language_id'))
                <span style="color:red;">{{ $errors->first('language_id') }}</span>
                @endif
              </div>
            </div><!--end col-md-6-->
        </div><!--row-->

        <div class="row">
            <div class="col-md-12">
              <div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
                <label for="content">المحتوى</label>
                <textarea class="form-control" name="content" rows="6">{{$Message->content}}</textarea>
                @if ($errors->has('content'))
                <span style="color:red;">{{ $errors->first('content') }}</span>
                @endif
              </div>
            </div><!--end col-md-12-->
        </div><!--row-->


        <div class="row">
        <div class="col-md-12">
            <hr />
            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> حفظ </button>
        </div><!--end col-md-12-->
        </div><!--row-->

        </form>
      </div><!--panel-body-->

    </div><!--panel panel-inverse-->
  </div><!--col-md-12-->
</div><!--row-->
@endsection