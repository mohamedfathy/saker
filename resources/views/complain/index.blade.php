@extends ('layouts.nosidebar')
@section('title', "الشكاوى و الاقتراحات")
@section ('content')

<div class="row">
<div class="col-md-12">
	<h1 class="text-center">التقييم و الشكاوى</h1>
</div><!--end col-md-12-->
</div><!--row-->
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">الشكاوى و الاقتراحات </h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>العميل</th>
              <th class="center">الرسالة</th>
              <th class="center">الاقتراح</th>
              <th class="center">التقييم</th>
              <th>تاريخ الارسال</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($Complain as $C)
            <tr>
              <td>{{ $C->agent_id }}</td>
              <td>{{ $C->message }}</td>
              <td>{{ $C->suggestion}}</td>
              <td>{{ $C->rating }}</td>
              <td>{{ $C->created_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection