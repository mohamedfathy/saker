@extends ('layouts.nosidebar') 
@section('title', "اضافة ") 
@section ('content')

<div class="row">
<div class="col-md-12">
	<h1 class="text-center">التقييم و الشكاوى</h1>
</div><!--end col-md-12-->
</div><!--row-->
<div class="row">
<hr />
</div><!--row-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove">
                        <i class="fa fa-times"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                </div>
                <!-- panel-heading-btn -->
                <h4 class="panel-title"> التقييم و الشكاوى </h4>
            </div>
            <!--panel-heading -->

            <div class="panel-body">
                <form action="/complains" method="POST" dir="rtl">
                    @csrf
                    <!-- Handel the Cross Site Request Forgery -->
					<input type="hidden" name="id" value="{{$id}}">
                   <div class="row">
                        <div class="col-md-6">
                          <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                            <label for="message">الرسالة</label>
                            <textarea class="form-control" name="message" rows="6"></textarea>
                            @if ($errors->has('message'))
                            <span style="color:red;">{{ $errors->first('message') }}</span>
                            @endif
                          </div>
                        </div><!--end col-md-6-->
                        <div class="col-md-6">
                          <div class="form-group {{ $errors->has('suggestion') ? ' has-error' : '' }}">
                            <label for="suggestion">الشكوى</label>
                            <textarea class="form-control" name="suggestion" rows="6"></textarea>
                            @if ($errors->has('suggestion'))
                            <span style="color:red;">{{ $errors->first('suggestion') }}</span>
                            @endif
                          </div>
                        </div><!--end col-md-126-->
                    </div><!--row-->
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('rating') ? ' has-error' : '' }}">
                                <label for="rating">التقييم</label>
                                <input type="number" min="0" max="5" name="rating" class="form-control" value="{{ old('rating') }}"> @if ($errors->has('rating'))
                                <span style="color:red;">{{ $errors->first('rating') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->
                    </div><!--row-->

 

                    <div class="row">
                        <div class="col-md-12">
                            <hr />
                            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> ارسال </button>
                        </div>
                        <!--end col-md-12-->
                    </div>
                    <!--row-->

                </form>
            </div>
            <!--panel-body-->

        </div>
        <!--panel panel-inverse-->
    </div>
    <!--col-md-12-->
</div>
<!--row-->
@endsection
