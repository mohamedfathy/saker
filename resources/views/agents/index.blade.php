@extends ('layouts.master')
@section('title', "مندوبي الشركات")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">مندوبي الشركات</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
          @guest
          @else 
          @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
        <h5 class="page-header"><a href="/agents/create" class="btn btn-primary btn-xs">اضافة مندوب شركة</a></h5><!-- page-header --><hr />
        @endif 
        @endguest
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>الاسم</th>
              <th>رقم الجوال</th>
              <th>البريد الالكتروني </th>
              <th>الجنسية</th>
              <th>مفعل</th>
              <th>اللون</th>
              @guest
              @else 
              @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
              <th>تعديل</th>
              <th>حذف</th>
              @endif 
              @endguest
            </tr>
          </thead>
          <tbody>
            @foreach ($Agent as $A)
            <tr>
              <td><a href="/agents/{{$A->id}}">{{ $A->first_name }} {{ $A->last_name }} {{ $A->thired_name }}</a></td>
              <td>{{ $A->phone }}</td>
              <td>{{ $A->email }}</td>
              <td>{{ $A->nationality->name }}</td>
              <td style="text-align:center;">
              @if ($A->is_active == 0)
              <a href="/agents/{{$A->id}}/activation">
              <input type="checkbox" data-render="switchery" data-theme="default" checked /> مفعل
              </a>
              @else
              <a href="/agents/{{$A->id}}/activation">
              <input type="checkbox" data-render="switchery" data-theme="default" />غير مفعل
              </a>
              @endif
              </td>
              <td>{{ $A->color }}</td>
              @guest
              @else 
              @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
              <td><a href="/agents/{{$A->id}}/edit" class="btn btn-success btn-xs">تعديل</a></td>
              <td><a href="/agents/{{$A->id}}/destroy" class="btn btn-danger btn-xs" onclick="return confirm('سيتم حذف البيانات نهائيا ... هل انت متاكد؟');">حذف</a></td>
              @endif 
              @endguest
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection
