@extends ('layouts.master')
@section('title', "اضافة مندوب شركة")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div><!-- panel-heading-btn -->
        <h4 class="panel-title"> اضافة مندوب شركة </h4>
      </div><!--panel-heading -->

      <div class="panel-body">
        <form action="/agents" method="POST" dir="rtl" enctype="multipart/form-data">
          @csrf <!-- Handel the Cross Site Request Forgery -->

        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name"> الاسم الاول </label>
                <input type="text" name="name" class="form-control"  value="{{ old('name') }}">
                @if ($errors->has('name'))
                <span style="color:red;">{{ $errors->first('name') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
            
                        <div class="col-md-6">
              <div class="form-group {{ $errors->has('last') ? ' has-error' : '' }}">
                <label for="last">الاسم الاوسط </label>
                <input type="text" name="last" class="form-control"  value="{{ old('last') }}">
                @if ($errors->has('name'))
                <span style="color:red;">{{ $errors->first('last') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
            
            
                        <div class="col-md-6">
              <div class="form-group {{ $errors->has('thired') ? ' has-error' : '' }}">
                <label for="thired"> الاسم الاخير </label>
                <input type="text" name="thired" class="form-control"  value="{{ old('thired') }}">
                @if ($errors->has('name'))
                <span style="color:red;">{{ $errors->first('thired') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
            
            

            <div class="col-md-6">
              <div class="form-group {{ $errors->has('national_number') ? ' has-error' : '' }}">
                <label for="national_number"> رقم الهوية </label>
                <input type="text" name="national_number" class="form-control"  value="{{ old('national_number') }}">
                @if ($errors->has('national_number'))
                <span style="color:red;">{{ $errors->first('national_number') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
        </div><!--row-->
        
        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone">رقم الجوال</label>
                <input type="text" name="phone" class="form-control"  value="{{ old('phone') }}">
                @if ($errors->has('phone'))
                <span style="color:red;">{{ $errors->first('phone') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->

            <div class="col-md-6">
              <div class="form-group {{ $errors->has('age') ? ' has-error' : '' }}">
                <label for="age">العمر</label>
                <input type="number" name="age" class="form-control"  value="{{ old('age') }}">
                @if ($errors->has('age'))
                <span style="color:red;">{{ $errors->first('age') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
        </div><!--row-->

        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email"> البريد الالكتروني</label>
                <input type="text" name="email" class="form-control"  value="{{ old('email') }}">
                @if ($errors->has('email'))
                <span style="color:red;">{{ $errors->first('email') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->

            <div class="col-md-6">
              <div class="form-group {{ $errors->has('nationality_id') ? ' has-error' : '' }}">
                <label for="nationality_id">الجنسية</label>
                <select class="form-control" name="nationality_id">
                  <option disabled selected value>...</option>
                  @foreach ($Nationality as $N) 
                  <option value="{{$N->id}}">{{$N->name}}</option>
                  @endforeach
                </select>
                @if ($errors->has('nationality_id'))
                <span style="color:red;">{{ $errors->first('nationality_id') }}</span>
                @endif
              </div>
            </div><!--end col-md-6-->
        </div><!--row-->

        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('barcode') ? ' has-error' : '' }}">
                <label for="barcode">رقم الباركود</label>
                <input type="text" name="barcode" class="form-control"  value="{{ old('barcode') }}">
                @if ($errors->has('barcode'))
                <span style="color:red;">{{ $errors->first('barcode') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->

            <div class="col-md-6">
              <div class="form-group {{ $errors->has('photo_url') ? ' has-error' : '' }}">
                <label for="photo_url">الصورة</label>
                <input type="file" name="photo_url" class="form-control">
                @if ($errors->has('photo_url'))
                <span style="color:red;">{{ $errors->first('photo_url') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
        </div><!--row-->

        <div class="row">
          <div class="col-md-6">
            <div class="form-group {{ $errors->has('is_active') ? ' has-error' : '' }}">
                <label for='is_active'>الحالة</label>
                <select class="form-control" name='is_active'>
                    <option value="0">غير نشط</option>
                    <option value="1">نشط</option>
                </select>
                @if ($errors->has('is_active'))
                <span style="color:red;">{{ $errors->first('is_active') }}</span>
                @endif
            </div>
            </div><!--end col-md-6-->

            <div class="col-md-6">
              <div class="form-group {{ $errors->has('color') ? ' has-error' : '' }}">
                <label for="color">نوع البطاقة</label>
                <select class="form-control" name="color">
                  <option disabled selected value>...</option>
                  <option value="gold">gold</option>
                  <option value="silver">silver</option>
                  <option value="normal">normal</option>
                </select>
                @if ($errors->has('color'))
                <span style="color:red;">{{ $errors->first('color') }}</span>
                @endif
              </div>
            </div><!--end col-md-6-->
        </div><!--row-->

        <div class="row">
            <div class="col-md-6">     
              <div class="form-group {{ $errors->has('company_number') ? ' has-error' : '' }}">
                <label for="company_number">رقم المنشاه</label>
                  <select class="multiple-select2 form-control" multiple="multiple" name="company_number[]">
                      @foreach ($companies as $C) 
                      <option value="{{$C->id}}">{{$C->number}}</option>
                      @endforeach
                  </select>
                  @if ($errors->has('company_number'))
                  <span style="color:red;">{{ $errors->first('company_number') }}</span>
                  @endif
              </div>
            </div><!--end col-md-6-->
              
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('card_number') ? ' has-error' : '' }}">
                <label for="card_number">رقم التعريف لكارد العمل</label>
                <input type="text" name="card_number" class="form-control"  value="{{ old('card_number') }}">
                @if ($errors->has('card_number'))
                <span style="color:red;">{{ $errors->first('card_number') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->
        </div><!--row-->


        <div class="row">
        <div class="col-md-12">
            <hr />
            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> حفظ </button>
        </div><!--end col-md-12-->
        </div><!--row-->

        </form>
      </div><!--panel-body-->

    </div><!--panel panel-inverse-->
  </div><!--col-md-12-->
</div><!--row-->
@endsection