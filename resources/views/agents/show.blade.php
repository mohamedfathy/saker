@extends ('layouts.master') 
@section('title', "مندوب الشركة") 
@section ('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">

            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove">
                        <i class="fa fa-times"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                </div>
                <!-- panel-heading-btn -->
                <h4 class="panel-title">مندوب الشركة</h4>
            </div>

            <div class="panel-body">
                <div class="profile-section">

                    <div class="profile-left">
                        <div class="profile-image" style="height:180px;background-image: url('{{ asset((($Agent->photo_url == NULL) ? '/uploads/agents/agent.png' : $Agent->photo_url) ) }}'); background-repeat: no-repeat;background-size: cover;">
                            <i class="fa fa-user hide"></i>
                        </div>
                        <!-- profile-image -->
                    </div>
                    <!-- profile-left -->

                    <div class="profile-right">
                        <div class="profile-info">
                            <div class="table-responsive">
                                <table class="table table-profile">
                                    <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h4>{{$Agent->name}}
                                                    <small>{{$Agent->type}}</small>
                                                </h4>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="highlight">
                                            <td class="field">الاسم الاول</td>
                                            <td>{{$Agent->first_name}}</td>
                                        </tr>
                                           <tr class="highlight">
                                            <td class="field">الاسم الاوسط </td>
                                            <td>{{$Agent->last_name}}</td>
                                        </tr>
                                        <tr class="highlight">
                                            <td class="field"> الاسم الاخير </td>
                                            <td>{{$Agent->thired_name}}</td>
                                        </tr>
                                        <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td class="field">رقم الهوية</td>
                                            <td>
                                                {{$Agent->national_number}}</td>
                                        </tr>
                                        <tr>
                                            <td class="field">التليفون</td>
                                            <td><i class="fa fa-mobile fa-lg m-r-5"></i> {{$Agent->phone}}</td>
                                        </tr>
                                        <tr>
                                            <td class="field">رقم التعريف لكارد العمل</td>
                                            <td>{{$Agent->card_number}}</td>
                                        </tr>
                                        <tr>
                                            <td class="field">العمر</td>
                                            <td>{{$Agent->age}}</td>
                                        </tr>
                                        <tr>
                                            <td class="field">البريد الالكترونى</td>
                                            <td>{{$Agent->email}}</td>
                                        </tr>
                                        <tr>
                                            <td class="field">الجنسية</td>
                                            <td>{{$Agent->nationality->name}}</td>
                                        </tr>
                                        <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td class="field">الباركود</td>
                                            <td>{{$Agent->barcode}}</td>
                                        </tr>
                                        <tr class="divider">
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td class="field">الحالة</td>
                                            <td>
                                                @if ($Agent->is_active === 0) {{'غير نشط'}} @else {{'نشط'}} @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="field"> التسجيل</td>
                                            <td>{{Carbon\Carbon::parse($Agent->created_at)->toDayDateTimeString()}}</td>
                                        </tr>
                                        <tr>
                                            <td class="field"> اللون</td>
                                            <td>{{$Agent->color}}</td>
                                        </tr>
                                        <tr>
                                            <td class="field">الشركات</td>
                                            <td>
                                            @foreach($Companies as $C)
                                             @foreach($C as $c) 
                                             
                                            	{{$c->name_ar }} ,
                                             @endforeach
                       						@endforeach
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- table-responsive -->
                        </div>
                        <!-- profile-info -->
                    </div>
                    <!-- profile-right -->
                </div>
                <!--profile-section -->
            </div>
            <!--panel-body -->

        </div>
        <!-- panel panel-inverse -->
    </div>
    <!-- col-md-12 -->
</div>
<!-- row -->


<!--<div class="row">-->

<!--    <div class="col-md-12 col-sm-12">-->

<!--    </div>-->

<!--    <div class="col-md-12">-->


        <!-- begin panel -->
<!--        <div class="panel panel-inverse">-->
<!--            <div class="panel-heading">-->
<!--                <div class="panel-heading-btn">-->
<!--                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand">-->
<!--                        <i class="fa fa-expand"></i>-->
<!--                    </a>-->
<!--                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload">-->
<!--                        <i class="fa fa-repeat"></i>-->
<!--                    </a>-->
<!--                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">-->
<!--                        <i class="fa fa-minus"></i>-->
<!--                    </a>-->


<!--                </div>-->
<!--                <h4 class="panel-title">المعاملات</h4>-->
<!--            </div>-->
<!--            <div class="panel-body">-->

<!--                <table id="data-table" class="table table-striped table-bordered nowrap" width="100%" dir="rtl">-->
<!--                    <thead>-->
<!--                        <tr>-->
<!--                            <th>رقم المعاملة</th>-->
<!--                            <th>التاريخ</th>-->

<!--                            {{-- <th>تعديل</th> --}}-->
<!--                            <th>حذف</th>-->
<!--                        </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                        @foreach($agetrans as $A) -->
<!--                          @foreach ($transactions as $T) -->
<!--                            @if($A->agent_id == $Agent->id && $A->transaction_id == $T->id)-->

<!--                              <tr>-->

<!--                                  <td>{{ $T->number }}</td>-->

<!--                                  <td>{{ $T->created_at }}</td>-->
<!--                                  {{-- <td>-->
<!--                                      <a href="/users/edit/{{$T->id}}" class="btn btn-success btn-xs"> تعديل </a>-->
<!--                                  </td> --}}-->
<!--                                  <td>-->
<!--                                      <a href="/users/destroy/{{$T->id}}" class="btn btn-danger btn-xs"> حذف </a>-->
<!--                                  </td>-->
<!--                              </tr>-->
<!--                            @endif-->
<!--                          @endforeach -->
<!--                        @endforeach-->
<!--                    </tbody>-->
<!--                </table>-->
<!--            </div>-->
<!--        </div>-->
        <!-- end panel -->
<!--    </div>-->

<!--</div>-->
<!-- end row -->
@endsection
