@extends ('layouts.master') 
@section('title', "المستخدمين") 
@section ('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove">
                        <i class="fa fa-times"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                </div>
                <!-- panel-heading-btn -->
                <h4 class="panel-title"> اضافة مستخدم  </h4>
            </div>
            <!--panel-heading -->

            <div class="panel-body">
                <form action="/admins" method="POST" dir="rtl" enctype="multipart/form-data">
                    @csrf
                    <!-- Handel the Cross Site Request Forgery -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name">الإسم الاول</label>
                                <input type="text" name="first_name" class="form-control" value="{{ old('first_name') }}"> @if ($errors->has('first_name'))
                                <span style="color:red;">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->

                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('second_name') ? ' has-error' : '' }}">
                                <label for="second_name">الإسم الاوسط</label>
                                <input type="text" name="second_name" class="form-control" value="{{ old('second_name') }}"> @if ($errors->has('second_name'))
                                <span style="color:red;">{{ $errors->first('second_name') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->
                    </div>
                    <!--row-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('third_name') ? ' has-error' : '' }}">
                                <label for="third_name">الإسم الأخير</label>
                                <input type="text" name="third_name" class="form-control" value="{{ old('third_name') }}"> @if ($errors->has('third_name'))
                                <span style="color:red;">{{ $errors->first('third_name') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->

                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">البريد الالكتروني</label>
                                <input type="text" name="email" class="form-control" value="{{ old('email') }}"> @if ($errors->has('email'))
                                <span style="color:red;">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->
                    </div>
                    <!--row-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password">الرمز المروري</label>
                                <input type="password" name="password" class="form-control" value="{{ old('password') }}"> @if ($errors->has('password'))
                                <span style="color:red;">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->


                        <div class="col-md-6">
                          <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                              <label for="password_confirmation">تأكيد الرمز المروري</label>
                              <input type="password" name="password_confirmation" class="form-control">
                              @if ($errors->has('password_confirmation'))
                              <span style="color:red;">{{ $errors->first('password_confirmation') }}</span>
                              @endif
                          </div>
                      </div><!--end col-md-6-->
                    </div>
                    <!--row-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('national_number') ? ' has-error' : '' }}">
                                <label for="national_number">رقم الهوية </label>
                                <input type="tel" name="national_number" pattern="^\d{3}-\d{4}-\d{7}-\d{1}$" class="form-control" id='identty'   value="{{ old('national_number') }}"> @if ($errors->has('national_number'))
                                <span style="color:red;">{{ $errors->first('national_number') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->

                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('role_id') ? ' has-error' : '' }}">
                                <label for="role_id">صلاحية المستخدم </label>
                                <select class="form-control" name="role_id">
                                    @foreach ($Role as $R)
                                    <option value="{{$R->id}}">{{$R->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role_id'))
                                <span style="color:red;">{{ $errors->first('role_id') }}</span>
                                @endif
                            </div>
                        </div>
                        <!--end col-md-6-->
                    </div>
                    <!--row-->

                    <div class="row">
                        {{-- <div class="col-md-6">
                            <div class="form-group {{ $errors->has('is_active') ? ' has-error' : '' }}">
                                <label for="is_active">الحالة</label>
                                <select class="form-control" name="is_active">
                                    <option value="1">نشط</option>
                                    <option value="0">غير نشط</option>
                                </select>
                                @if ($errors->has('is_active'))
                                <span style="color:red;">{{ $errors->first('is_active') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6--> --}}


                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('third_name') ? ' has-error' : '' }}">
                                <label for="job">الرقم الوظيفي</label>
                                <input type="text" name="job" class="form-control" value="{{ old('job') }}"> @if ($errors->has('job'))
                                <span style="color:red;">{{ $errors->first('job') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->

                        <div class="col-md-6">
                          <div class="form-group {{ $errors->has('is_active') ? ' has-error' : '' }}">
                              <label for='is_active'>حالة المستخدم</label>
                              <select class="form-control" name='is_active'>
                                  <option value="0">غير نشط</option>
                                  <option value="1">نشط</option>
                              </select>
                              @if ($errors->has('is_active'))
                              <span style="color:red;">{{ $errors->first('is_active') }}</span>
                              @endif
                          </div>
                          </div><!--end col-md-6-->

                    </div>
                    <!--row-->
						<div class="row">
                        <div class="col-md-6">
                          <div class="form-group {{ $errors->has('photo_url') ? ' has-error' : '' }}">
                              <label for="photo_url">صورة المستخدم</label>
                              <input type="file" accept="image/x-png,image/gif,image/jpeg" name="photo_url" class="form-control"> @if ($errors->has('photo_url'))
                              <span style="color:red;">{{ $errors->first('photo_url') }}</span>
                              @endif
                          </div>
                          <!--end form-group-->
                      </div>
                      <!--end col-md-6-->
						</div>

                    <div class="row">
                        <div class="col-md-12">
                            <hr />
                            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> حفظ </button>
                        </div>
                        <!--end col-md-12-->
                    </div>
                    <!--row-->

                </form>
            </div>
            <!--panel-body-->

        </div>
        <!--panel panel-inverse-->
    </div>
    <!--col-md-12-->
</div>
<!--row-->
@endsection
