@extends ('layouts.master')
@section('title', "المستخدمين")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">المستخدمين</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
          @guest
          @else 
          @if(Auth::user()->role_id == 1)
        <h5 class="page-header"><a href="/admins/create" class="btn btn-primary btn-xs">اضافة مستخدم</a></h5><!-- page-header --><hr />
        @endif 
        @endguest
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>الإسم </th>
              <th>البريد الالكتروني</th>
              <th>رقم الهوية </th>
              <th>صلاحية المستخدم </th>
              <th>مفعل</th>
              <th>الرقم الوظيفي</th>
              @guest
              @else 
              @if(Auth::user()->role_id == 1)
              <th>تعديل</th>
              <th>حذف</th>
              @endif 
              @endguest
            </tr>
          </thead>
          <tbody>
            @foreach ($Admin as $A)
            <tr>
              <td><a href="/admins/{{$A->id}}">{{ $A->first_name }} {{ $A->second_name }} {{ $A->third_name }}</a></td>
              <td>{{ $A->email }}</td>
              <td>{{ $A->national_number }}</td>
              <td>{{ $A->role->name }}</td>
              <td style="text-align:center;">
              
              
              @if ($A->is_active == 1)
              <a href="/admins/{{$A->id}}/activation">
              <input type="checkbox" data-render="switchery" data-theme="default" checked /> مفعل
              </a>
              @else
              <a href="/admins/{{$A->id}}/activation">
              <input type="checkbox" data-render="switchery" data-theme="default" />غير مفعل
              </a>
              @endif
              </td>
              <td>{{ $A->number_job }}</td>
              @guest
              @else 
              @if(Auth::user()->role_id == 1)
              <td><a href="/admins/{{$A->id}}/edit" class="btn btn-success btn-xs">تعديل</a></td>
              <td><a href="/admins/{{$A->id}}/destroy" class="btn btn-danger btn-xs" onclick="return confirm('سيتم حذف البيانات نهائيا ... هل انت متاكد؟');">حذف</a></td>
              @endif 
              @endguest
             
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection