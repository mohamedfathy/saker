@extends ('layouts.master')
@section('title', "تعديل رسالة عميل")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div><!-- panel-heading-btn -->
        <h4 class="panel-title"> تعديل رسالة عميل </h4>
      </div><!--panel-heading -->

      <div class="panel-body">
        <form action="/agentMessages/{{$AgentMessage->id}}" method="POST" dir="rtl">
          @method('PUT') <!-- Spoofing Form Methods -->
          @csrf          <!-- Handel the Cross Site Request Forgery -->
          <input type="hidden" name="id" value="{{$AgentMessage->id}}">

        <div class="row">
          <div class="col-md-6">
            <div class="form-group {{ $errors->has('message_type_id') ? ' has-error' : '' }}">
              <label for="message_type_id">نوع الرسالة</label>
              <select class="form-control" name="message_type_id">
                <option disabled selected value>...</option>
                @foreach ($MessageType as $MT)) 
                <option value="{{$MT->id}}" @if ( $AgentMessage->message_type_id === $MT->id) {{'selected'}} @endif>{{$MT->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('message_type_id'))
              <span style="color:red;">{{ $errors->first('message_type_id') }}</span>
              @endif
            </div>
          </div><!--end col-md-6-->

          <div class="col-md-6">
            <div class="form-group {{ $errors->has('agent_id') ? ' has-error' : '' }}">
              <label for="agent_id">العميل</label>
              <select class="form-control" name="agent_id">
                <option disabled selected value>...</option>
                @foreach ($Agent as $A)) 
                <option value="{{$A->id}}" @if ( $AgentMessage->agent_id === $A->id) {{'selected'}} @endif>{{$A->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('agent_id'))
              <span style="color:red;">{{ $errors->first('agent_id') }}</span>
              @endif
            </div>
          </div><!--end col-md-6-->
        </div><!--row-->

        {{-- <div class="row">
          <div class="col-md-12">
            <div class="form-group {{ $errors->has('admin_id') ? ' has-error' : '' }}">
              <label for="admin_id">المدير</label>
              <select class="form-control" name="admin_id">
                <option disabled selected value>...</option>
                @foreach ($Admin as $A)) 
                <option value="{{$A->id}}" @if ( $AgentMessage->admin_id === $A->id) {{'selected'}} @endif>{{$A->first_name}}</option>
                @endforeach
              </select>
              @if ($errors->has('admin_id'))
              <span style="color:red;">{{ $errors->first('admin_id') }}</span>
              @endif
            </div>
          </div><!--end col-md-12-->
        </div><!--row--> --}}

        <div class="row">
            <div class="col-md-12">
              <div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
                <label for="content">المحتوى</label>
                <textarea class="form-control" rows="5" name="content">{{$AgentMessage->content}}</textarea>
                @if ($errors->has('content'))
                <span style="color:red;">{{ $errors->first('content') }}</span>
                @endif
              </div>
            </div><!--end col-md-12-->
        </div><!--row-->

        <div class="row">
        <div class="col-md-12">
            <hr />
            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> حفظ </button>
        </div><!--end col-md-12-->
        </div><!--row-->

        </form>
      </div><!--panel-body-->

    </div><!--panel panel-inverse-->
  </div><!--col-md-12-->
</div><!--row-->
@endsection