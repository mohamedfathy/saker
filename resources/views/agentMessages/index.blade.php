@extends ('layouts.master')
@section('title', "رسائل مندوب شركة")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">رسائل مندوب شركة</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
               <!--POP Add this to show  sendMessageFalid -->
      @if(Session::get("sendMessageSucc"))
      <<div class="alert alert-success fade in m-b-15 text-center">
                @foreach (Session::get("sendMessageSucc") as $message) 
                    <strong>  {{$message}}!</strong>
                @endforeach
								

								<span class="close" data-dismiss="alert">×</span>
							</div>
      @endif
      <!--POP End  -->
                            
          @guest
          @else 
          @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
        <h5 class="page-header"><a href="/agentMessages/create" class="btn btn-primary btn-xs">إرسال الرسائل اللى مندوبى الشركات </a>
       <a href="" class="btn btn-info btn-xs"> {{$balance}} عدد الرسايل المتبقية </a></h5><!-- page-header -->
        </h5><!-- page-header --><hr />

        @endif 
        @endguest
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>الرقم</th>
              <th>المحتوى</th>
              <th>العميل</th>
              <th>المدير</th>
              <th>التاريخ</th>
              @guest
              @else 
              @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
              <th>حذف</th>
              @endif 
              @endguest
            </tr>
          </thead>
          <tbody>
            @foreach ($AgentMessage as $AM)
            <tr>
              <td>{{ $AM->id }}</td>
              <td style="width:300px;">{{ $AM->message->content }}</td>
              <td>{{ $AM->agent->first_name }}</td>
              <td>{{ $AM->admin->first_name }}</td>
              <td>{{ Carbon\Carbon::parse($AM->created_at)->toDayDateTimeString() }}</td>
              @guest
              @else 
              @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
              <td><a href="/agentMessages/{{$AM->id}}/destroy" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to Remove?');">حذف</a></td>
              @endif 
              @endguest
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection