@extends ('layouts.master')
@section('title', "اضافة رسالة عميل")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div><!-- panel-heading-btn -->
        <h4 class="panel-title">ارسال رسالة</h4>
      </div><!--panel-heading -->

      <div class="panel-body">

          <!--POP Add this to show  sendMessageFalid -->
      @if(Session::get("sendMessageFalid"))
      <div class="alert alert-danger fade in m-b-15 text-search">
								<strong>  {{Session::get("sendMessageFalid")}}!</strong>

								<span class="close" data-dismiss="alert">×</span>
							</div>
      @endif
      <!--POP End  -->

        <form action="/agentMessages" method="POST" dir="rtl">
          @csrf <!-- Handel the Cross Site Request Forgery -->

        <div class="row">
            <div class="col-md-6">
              <div class="form-group  {{ $errors->has('message_id') ? ' has-error' : '' }}">
                <label for="message_id">المحتوى</label>
                <select class="default-select2 form-control" name="message_id">
                  <option disabled selected value>...</option>
                  @foreach ($Messages as $M) 
                  <option value="{{$M->id}}">{{$M->content}}</option>
                  @endforeach
                </select>
                @if ($errors->has('message_id'))
                <span style="color:red;">{{ $errors->first('message_id') }}</span>
                @endif
              </div>
            </div><!--end col-md-6-->


            <div class="col-md-6">
              <div class="form-group {{ $errors->has('agent_id') ? ' has-error' : '' }}">
                <label for="agent_id">العميل</label>
                <select class="multiple-select2 form-control" multiple="multiple" name="agent_id[]">
                  @foreach ($Agent as $A) 
                  <option value="{{ $A->id}}">{{ $A->first_name}}</option>
                  @endforeach
                </select>
                @if ($errors->has('agent_id'))
                <span style="color:red;">{{ $errors->first('agent_id') }}</span>
                @endif
              </div>
            </div><!--end col-md-6-->
        </div><!--row-->


        <div class="row">
        <div class="col-md-12">
            <hr />
            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> ارسال </button>
        </div><!--end col-md-12-->
        </div><!--row-->

        </form>
      </div><!--panel-body-->

    </div><!--panel panel-inverse-->
  </div><!--col-md-12-->
</div><!--row-->
@endsection