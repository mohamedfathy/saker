@extends ('layouts.master')
@section('title', "مدفوعات عميل")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">مستحقات مندوب شركة</h4>
      </div><!--panel-heading-->

      <div class="panel-body">              

        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>الشهر</th>
              <th>الاجمالى</th>
              <th>تم الدفع</th>
              <th>تاخير استلام</th>
              <th>دفع المستحقات</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($AgentPayment as $AP)
            <tr>
              <td>{{ $AP->month }}</td>
              <td>{{ $AP->total }}</td>
              <td>
                @if ($AP->is_paid == 0)
                لم يتم الدفع
                @else
                تم الدفع
                @endif 
			</td>
            <td>
            	@if ($AP->is_late == 0)
                 غير متاخر
                @else
                  متاخر
           		@endif 
            </td>
            <td style="text-align:center;">
              @if ($AP->is_paid == 0)
              <a href="/agentPayments/{{$AP->id}}/getPayed">
              <input type="checkbox" data-render="switchery" data-theme="default" checked /> لم يتم الدفع
              </a>
              @else
              <a href="/agentPayments/{{$AP->id}}/getPayed">
              <input type="checkbox" data-render="switchery" data-theme="default" />تم الدفع
              </a>
              @endif
            </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection