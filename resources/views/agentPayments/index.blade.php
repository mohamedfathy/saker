@extends ('layouts.master')
@section('title', "مدفوعات العملاء")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">مستحقات مندوب شركة</h4>
      </div><!--panel-heading-->

      <div class="panel-body">              

        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>#</th>
              <th>مندوب الشركة</th>
              <th>عرض</t
            </tr>
          </thead>
          <tbody>
            @foreach ($Agent as $A)
            <tr>
              <td>{{ $A->id }}</td>
              <td>{{ $A->first_name }} {{ $A->last_name }}</td>
              <td><a class="btn btn-primary btn-xs" href="/agentPayments/{{$A->id}}">عرض</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection