@extends('layouts.login')

@section('content')
<!-- begin login -->
<div class="login login-with-news-feed">
    <!-- begin news-feed -->
    <div class="news-feed" style="direction:rtl;">
        <div class="news-image">
            <img src="assets/img/login-bg/bg-2.jpg" data-id="login-cover-image" alt="" />
        </div>
        <div class="news-caption">
            <h4 class="caption-title"><i class="fa fa-diamond text-success"></i>الصقر </h4>
            <p>
                صفحة تسجيل الدخول
            </p>
        </div>
    </div>
    <!-- end news-feed -->
    <!-- begin right-content -->
    <div class="right-content" style="direction:rtl;">
        <!-- begin login-header -->
        <div class="login-header">
            <div class="brand">
                <span class="logo"></span> الصقر
                <small>صفحة تسجيل الدخول</small>
            </div>
            <div class="icon">
                <i class="fa fa-sign-in"></i>
            </div>
        </div>
        <!-- end login-header -->
        <!-- begin login-content -->
        <div class="login-content">
            <form action="{{ route('login') }}" method="POST" class="margin-bottom-0">
            	@csrf
                <div class="form-group m-b-15">
                	<label for="email">البريد الالكترونى</label>
                    <input id="email" type="email" class="form-control input-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus />

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                </div>
                <div class="form-group m-b-15">
					<label for="password">كلمة المرور</label>
                    <input id="password" type="password" name="password" class="form-control input-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" required />
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif 
                </div>
                <div class="checkbox m-b-30">
                    <label>
                        <input type="checkbox" /> تذكرنى
                    </label>
                </div>
                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">تسجيل دخول الموظفين</button>
                </div>
                <hr />
                	<a href="/agentPhone" class="btn btn-success btn-block btn-lg">تسجيل العملاء المميزين</a>
                <hr />
                <p class="text-center">
                    مركز الصقر لخدمات رجال الأعمال  &copy; <?php echo date('Y'); ?>
                </p>
            </form>
        </div>
        <!-- end login-content -->
    </div>
    <!-- end right-container -->
</div>
<!-- end login -->
@endsection