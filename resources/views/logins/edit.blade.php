@extends ('layouts.master')
@section('title', "تعديل عميل")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

      <div class="panel-heading">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div><!-- panel-heading-btn -->
        <h4 class="panel-title"> تعديل عميل </h4>
      </div><!--panel-heading -->

      <div class="panel-body">
        <form action="/roles/{{$role->id}}" method="POST" dir="rtl" enctype="multipart/form-data">
          @method('PUT') <!-- Spoofing Form Methods -->
          @csrf          <!-- Handel the Cross Site Request Forgery -->
          <input type="hidden" name="id" value="{{$role->id}}">

        <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
                <label for="role">الدور</label>
                <input type="text" name="role" class="form-control"  value="{{$role->name}}">
                @if ($errors->has('role'))
                <span style="color:red;">{{ $errors->first('role') }}</span>
                @endif
              </div><!--end form-group-->
            </div><!--end col-md-6-->

        </div><!--row-->


        <div class="row">
        <div class="col-md-12">
            <hr />
            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> حفظ </button>
        </div><!--end col-md-12-->
        </div><!--row-->

        </form>
      </div><!--panel-body-->

    </div><!--panel panel-inverse-->
  </div><!--col-md-12-->
</div><!--row-->
@endsection