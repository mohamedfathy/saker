@extends ('layouts.master')
@section('title', "حظر المستخدمين")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">حظر المستخدمين</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        {{-- <h5 class="page-header"><a href="/logins/create" class="btn btn-primary btn-xs">اضافة دور</a></h5><!-- page-header --><hr /> --}}
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              {{-- <th>الرقم</th> --}}
              <th>IP</th>
              <th>اسم الجهاز</th>
              <th>رقم المدير</th>
              <th>التاريخ</th>
              <th>الحالة</th>

              {{-- <th>تعديل</th> --}}
              <th>حذف</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($logs as $L)
            <tr>
              {{-- <td>{{ $R->id }}</td> --}}
              <td>{{ $L->ip }}</td>
              <td>{{ $L->device }}</td>
              <td>{{ $L->admin_id }}</td>
              <td>{{ $L->created_at }}</td>
              <td style="text-align:center;">
                  @if ($L->paned == 1)
                  <a href="/logins/{{$L->id}}/activation" class="btn btn-danger btn-xs">محجوب</a>
                  @else
                  <a href="/logins/{{$L->id}}/activation" class="btn btn-success btn-xs">مصرح</a>
                  @endif
                </td>
              {{-- <td>{{ $L->paned }}</td> --}}

              {{-- <td><a href="/logins/{{$L->id}}/edit" class="btn btn-success btn-xs">تعديل</a></td> --}}
              <td><a href="/logins/{{$L->id}}/destroy" class="btn btn-danger btn-xs">حذف</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection