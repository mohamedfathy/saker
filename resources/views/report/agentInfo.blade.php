@extends ('layouts.master')
@section('title', "بيانات مندوب")
@section ('content')

<div class="row">
    <div class="col-md-3">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>اسم المندوب : </strong> {{$Agent->first_name }} {{$Agent->last_name }} {{$Agent->thired_name }}
    </div>
    </div>
    <!--end col-md-3-->
    
    <div class="col-md-3">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>عدد معاملات المندوب : </strong> {{$transactionCount}}
    </div>
    </div>
    <!--end col-md-3-->
    
    <div class="col-md-3">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong> تاريخ الانضمام : </strong> {{$Agent->created_at}}
    </div>
    </div>
    <!--end col-md-3-->
    
    <div class="col-md-3">
    <div class="alert alert-info fade in m-b-15" dir="rtl">
    	<strong>رصيد الشهر المستحق : </strong> {{$deservedMoney}}
    </div>
    </div>
    <!--end col-md-3-->
</div>
<!--row-->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">معاملات المندوب الشهرية </h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>رقم المعاملة</th>
              <th>تاريخ المعاملة</th>
              <th>تمت العملية بنجاح </th>
              <th>التكلفة</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($allMonthTransactions as $AMT)
              <tr>
              <td>{{ $AMT->number }}</td>
              <td>{{ $AMT->created_at }}</td>
              <td>{{ $AMT->is_successful }}</td>
              <td>{{ $AMT->cost }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->

@endsection