@extends ('layouts.master')
@section('title', "التفاصيل")
@section ('content')



<h2 style="text-align:center;">التفاصيل</h2>
<!-- begin row -->
<div class="row">
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
        <h4>اجمالى النفقات اليومية</h4>
        <p>{{$dailyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات الشهرية</h4>
      <p>{{$monthlyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-green">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى النفقات السنوية</h4>
      <p>{{$yearlyPayments}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->



<h2 style="text-align:center;">المعاملات</h2>
<!-- begin row -->
<div class="row">
 
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
        <h4>اجمالى المعاملات اليومية</h4>
        <p>{{$dailyTransaction}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
      <h4>اجمالى المعاملات الشهرية</h4>
      <p>{{$monthlyTransactions}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fa fa-exchange"></i></div>
      <div class="stats-info">
      <h4>اجمالى المعاملات السنوية</h4>
      <p>{{$yearlyTransactions}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->


<h2 style="text-align:center;">الرسائل</h2>
<!-- begin row -->
<div class="row">
 
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
        <h4>اجمالى الرسائل اليومية</h4>
        <p>{{$dailyMessage}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
      <h4>اجمالى الرسائل الشهرية</h4>
      <p>{{$monthlyMessages}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-purple">
      <div class="stats-icon"><i class="fa fa-comment-o"></i></div>
      <div class="stats-info">
      <h4>اجمالى الرسائل السنوية</h4>
      <p>{{$yearlyMessages}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->


<h2 style="text-align:center;">مستحقات غير مدفوعة </h2>
<!-- begin row -->
<div class="row">
 
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
        <h4>اجمالى المستحقات غير المدفوعة يومية</h4>
        <p>{{$dailyPaymentsNotPayed}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى المستحقات غير المدفوعة شهرية</h4>
      <p>{{$monthlyPaymentsNotPayed}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  <!-- begin col-3 -->
  <div class="col-md-4 col-sm-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fa fa-money"></i></div>
      <div class="stats-info">
      <h4>اجمالى المستحقات غير المدفوعة سنوية</h4>
      <p>{{$yearlyPaymentsNotPayed}}</p>  
      </div>
      <div class="stats-link">

      </div>
    </div>
  </div>
  <!-- end col-3 -->
  
</div><!-- end row -->
@endsection