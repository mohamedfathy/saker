@extends ('layouts.master')
@section('title', "تقارير العملاء")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">تقارير العملاء</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>#</th>
              <th>اسم العميل </th>
              <th>المدفوع يومى </th>
              <th>المدفوع شهرى </th>
              <th>المدفوع سنوى </th>
              <th>المستحق يومى </th>
              <th>المستحق شهرى </th>
              <th>المستحق سنوى </th>
              <th>تفاصيل المعاملات </th>
            </tr>
          </thead>
          <tbody>

            @foreach ($agents as $oneAgent)
              <tr>
              <td>{{ $oneAgent['id'] }}</td>
              <td><a href="agents/{{$oneAgent['id']}}">{{ $oneAgent['name'] }}</a></td>
              <td>{{ $oneAgent['DP'] }}</td>
              <td>{{ $oneAgent['MP'] }}</td>
              <td>{{ $oneAgent['YP'] }}</td>
              <td>{{ $oneAgent['DUP'] }}</td>
              <td>{{ $oneAgent['MUP'] }}</td>
              <td>{{ $oneAgent['YUP'] }}</td>
              <td><a class="btn btn-primary btn-xs" href="agentReport/{{$oneAgent['id']}}"> عرض</a></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection