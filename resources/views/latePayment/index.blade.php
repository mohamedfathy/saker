@extends ('layouts.master')
@section('title', "مستحقات متاخرة")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">مستحقات متاخرة</h4>
      </div><!--panel-heading-->

      <div class="panel-body">              
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>#</th>
              <th>مندوب الشركة </th>
              <th>عن شهر  </th>
              <th> المبلغ المستحق </th>
              <th>تاريخ الاستلام </th>
              <th>تم الاستلام </th>
              <th>تسليم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($LatePayment as $LP)
            <tr>
              <td>{{ $LP->id }}</td>
              <td>{{ $LP->agent->first_name }} {{ $LP->agent->last_name }}</td>
              <td>{{ $LP->month }}</td>
              <td>{{ $LP->cost }}</td>
              <td>{{ $LP->receive_date }}</td>
              <td>@if ($LP->received == 1) نعم @else لا @endif</td>>
              <td>
              @if ($LP->received == 0)  
              <a class="btn btn-primary btn-xs" href="/latePayments/{{$LP->id}}/getPayed" onclick="return confirm('ستقوم بالدفع كاش للمندوب الان . هل انت متاكد')" >تسليم</a> 
              @else 
              <a class="btn btn-danger btn-xs" >تم التسليم</a> 
              @endif
              </td>         
              </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection