@extends ('layouts.master') 
@section('title', "تسجيل معاملة") 
@section ('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="form-stuff-3">

            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove">
                        <i class="fa fa-times"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                </div>
                <!-- panel-heading-btn -->
                <h4 class="panel-title"> تسجيل معاملة </h4>
            </div>
            <!--panel-heading -->

            <div class="panel-body">
                <form action="/transactions" method="POST" dir="rtl" enctype="multipart/form-data">
                    @csrf
                    <!-- Handel the Cross Site Request Forgery -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('number') ? ' has-error' : '' }}">
                                <label for="number">رقم المعاملة</label>
                                <input type="text" name="number" class="form-control" value="{{ old('number') }}"> @if ($errors->has('number'))
                                <span style="color:red;">{{ $errors->first('number') }}</span>
                                @endif
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-6-->
                        
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('agent_id') ? ' has-error' : '' }}">
                                <label for="agent_id">اسم المندوب </label>
                                <select class="form-control" name="agent_id">
                                    @foreach ($Agent as $A)
                                    <option value="{{$A->id}}">{{$A->first_name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('agent_id'))
                                <span style="color:red;">{{ $errors->first('agent_id') }}</span>
                                @endif
                            </div>
                        </div>
                        <!--end col-md-6-->
                    </div>
                    <!--row-->
                    
                    


                    <div class="row">
                        <div class="col-md-12">
                            <hr />
                            <button type="submit" class="btn btn-sm btn-primary m-r-5 pull-left"> حفظ </button>
                        </div>
                        <!--end col-md-12-->
                    </div>
                    <!--row-->

                </form>
            </div>
            <!--panel-body-->

        </div>
        <!--panel panel-inverse-->
    </div>
    <!--col-md-12-->
</div>
<!--row-->
@endsection
