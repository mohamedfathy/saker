@extends ('layouts.master')
@section('title', "المعاملات")
@section ('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-inverse">

      <div class="panel-heading">
          <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div><!--panel-heading-btn-->
          <h4 class="panel-title">المعاملات</h4>
      </div><!--panel-heading-->

      <div class="panel-body">
        <table id="data-table" class="table table-striped table-bordered" dir="rtl" >
          <thead>
            <tr>
              <th>#</th>
              <th>اسم العميل </th>
              <th>رقم المعاملة</th>
              <th>تاريخ المعاملة</th>
              <th>تمت المعاملة بنجاح</th>
              <th>التكلفة</th>
              <th>تعديل</th>
              <th>حذف</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($Transaction as $T)
            <tr>
              <td>{{ $T->id }}</td>
              <td>{{ $T->agent->first_name }}</td>
              <td>{{ $T->number }}</td>
              <td>{{ $T->created_at }}</td>
              <td>{{ ($T->is_successful == 1) ? 'نعم' : 'لا' }}</td>
              <td>{{ $T->cost }}</td>
              <td><a href="/transactions/{{$T->id}}/edit" class="btn btn-success btn-xs">تعديل</a></td>
              <td><a href="/transactions/{{$T->id}}/destroy" class="btn btn-danger btn-xs" onclick="return confirm('سيتم حذف البيانات نهائيا ... هل انت متاكد؟');">حذف</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- panel-body -->
      
    </div><!--panel panel-inverse -->
  </div><!-- col-md-12 -->
</div><!-- end row -->
@endsection