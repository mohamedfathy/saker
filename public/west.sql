-- MySQL dump 10.16  Distrib 10.1.35-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: saqara
-- ------------------------------------------------------
-- Server version	10.1.35-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `second_name` varchar(50) NOT NULL,
  `third_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `photo_url` varchar(200) NOT NULL,
  `national_number` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `remember_token` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_job` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `number_job` (`number_job`),
  KEY `FK_admin_role` (`role_id`),
  CONSTRAINT `FK_admin_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'ahmed','ali','ahmed','aaa@yahoo.com','$2y$10$/2dTkNz0l8stnO57q3BFBuuVKDz0JsjTkyJPjscD1lbhbScBo2Bo2','http://127.0.0.1:8000/uploads/agents/vdRgQE4EofNNa17YDP4h58nEpYc6Q6.jpg','12345678909876',1,1,'2018-07-01','EXP9vPk9d5kHP30TGGaNuNg1FTmb5jmUVfjKwQKW0Jhcw6fVcdo5PGzICwfy','20'),(2,'علي','حمدي','ممتاز','alokah@gmail.com','$2y$10$pGqWJn80ZrWYZvk0Un6ZYOh24DK1HfHSBuYhupqogWJs7lRvxsmGa','http://127.0.0.1:8000/uploads/agents/JyKvANQWzwFckeIxwfwjGEUBwctF1U.jpg','45321678909876',2,0,'2018-07-01','UG6b3HfT5zG7LJk3DrYXqOYQQtSWMBK0AzgnQk0l9xo29w05nikzxyWUApKq','10'),(3,'اشرف','زكي','عبدالله','ashrof@yahoo.com','$2y$10$Ze152vsFFwaiM9p5sLU49.7sYi90e3DqvdManfIIyAqL6k31VAxgO','http://127.0.0.1:8000/uploads/agents/MryxOZ1QBU7s3QeXbIGFvXFeApRwpv.jpg','54389076123459',2,1,'2018-07-01','ABAgnb1jYYI1GZec1jpIreEEQAuue1VNpkDSuiv7aXcpDuhJbpOe3y1mHbGQ','55'),(4,'jmw','mmw','mmw','mo@yahoo.com','$2y$10$Zy/nFVGQBnVmzIcKn1ZIjeVJ0IxLuplcCPQBLRrWX4tg0gG5xtzma','https://saqara.magdsoft.com/uploads/agents/LCJzRsP1IyzctWkd0Iux7yPvjfdNPZ.jpeg','564787489',2,0,'2018-07-02','HS2tKULdxzH5xxO6Ds9i4E8Srh3MiULg7tEvIBaypdamdkPuirPlVMoyPtdm','555'),(5,'Iiiiiiiiiiiiiiiiiiiiii','IIIIIIIIIIIIIIIIIIIIIIIIIIII','IIIIIIIIIIIIIII','abdelrhman_sayed22@yahoo.com','$2y$10$BEDizsbMyCaCiDd1ucG5duiNpFOr5PmMAtl5IdzdqIjOO9udOnbIG','https://saqara.magdsoft.com/uploads/agents/RTtp3fUHM2JwxaxIqTdCuIUtVsrJww.jpg','156748994798751456',2,1,'2018-07-07',NULL,'777'),(6,'abdo','dfdsfdsf','dfdsfds','ceo@gmail.com','$2y$10$dId.gBSHp6XXfQ5Jqp0IKOlJPoS046qqGWdlvUG.8mC2JPTVKD/L6','https://saqara.magdsoft.com/uploads/agents/AzzRaYDsOxyjBu0vv2iZw7xks2gqJy.png','6546549876546546',2,1,'2018-07-25',NULL,'54gfd'),(7,'Islam','shaban','Ibrahim','islam@gmail.com','$2y$10$1MowETlpmCwAq.uuykA7g.5dSaTiHLrMY3pGHZLaASj2ThO.WVt0W','https://saqara.magdsoft.com/uploads/agents/WiQhlT9AecKDHQiWnQzUi3eZhWrYgX.png','292-9929-2989376-8',1,1,'2018-07-29','51GjkTQbDJ4qrzRRKlhlwe1VeAYyGCcFNrnjVO5lmGql8GW5CbSRW1niIIRx','123455'),(8,'232','321','654','987','$2y$10$1mscm1ImDpFLElC.Mz/.keRsTyvmuSxvn7Wq8Rbay6Jg7/3sFlVfa','https://saqara.magdsoft.com/uploads/agents/73KOkNaJM88MI1mB0Sb8GfkHbX7emh.png','253-6698-8741221-5',1,0,'2018-08-05',NULL,'22'),(9,'Osama','abdo','ceio','abdo@yahoo.com','$2y$10$StEqsZnYx3IJxOv9rBLtV.97JyhtdkjNDZRQmTWfXEg60o1RtZU7m','https://saqara.magdsoft.com/uploads/agents/0Ciz030DAz5fDy9LmLBpXsYRm3a8gL.jpg','548-6798-7987984-7',2,1,'2018-08-26','pANJZ7YCHLUcIRMdj8gC9HYEUKscyAz7Cnjfre3jO13cJCPjhcyXzYc6w0AV','2222'),(10,'امنة','محمد','راشد','midousa50@yahoo.com','$2y$10$9fbKExM/ddaaqD7AlCJ/3OiFDjK9qUF3MJFiY8rv1.RgC1Y.LMywa','https://saqara.magdsoft.com/uploads/agents/YpxfWdoRHwbgqJt7UDXdETbhSnb7Vd.png','797-8454-6546545-6',3,0,'2018-09-09','C2cGqlucTxU2N5JycHKQ5yya72EK0RZsDiUxzhEwoWxmpzIErWWSi18qB3Pu','50505');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents`
--

DROP TABLE IF EXISTS `agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `national_number` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `age` int(2) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nationality_id` int(11) NOT NULL,
  `barcode` text,
  `photo_url` varchar(200) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `color` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `thired_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `third_name` (`id`),
  KEY `FK_agent_nationality` (`nationality_id`),
  CONSTRAINT `FK_agent_nationality` FOREIGN KEY (`nationality_id`) REFERENCES `nationalites` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents`
--

LOCK TABLES `agents` WRITE;
/*!40000 ALTER TABLE `agents` DISABLE KEYS */;
INSERT INTO `agents` VALUES (1,'محسن ممتاز','+0201008292985','97142869913',30,'wardah@gmail.com',2,'2345678889','http://127.0.0.1:8000/uploads/agents/EELJSA9mbzxqLmGvBgzjl9gUSacbEl.jpg',1,'2018-07-30','silver','0','0'),(4,'amr','1564847984984','00201100434899',27,'amr@yahoo.com',5,'584','https://saqara.magdsoft.com/uploads/agents/HHE0l6Ocw7FGk4rxyIUZGcFzCanPs0.jpg',0,'2018-07-10','gold','ddd','xxxx'),(6,'Students','65654654654','56456546',20,'admin1@admin.com',1,'54654654','https://saqara.magdsoft.com/uploads/agents/frvrN01RuFQ5mgCTKPXLYvSFyZcb2u.png',1,'2018-07-25','gold','Students','Students'),(7,'احمد','123456789077','123456789',20,'ahmed@gmail.com',2,'1234567890','https://saqara.magdsoft.com/uploads/agents/KVXJRRQjoAZDKEqTd2eiYvZO115SeG.jpg',1,'2018-07-29','gold','حسن','محمد'),(8,'عبد الرحمن','00098876545678','0188988765',44,'mohamed@gmail.com',4,'019887654567','https://saqara.magdsoft.com/uploads/agents/J9LeYP26kQLbosu3RF5CnEQNYd3D4V.jpg',1,'2018-08-08','normal','سيد','صلاح'),(9,'ali','213564789952121','01234567',22,'testteam@magdsoft.com',2,'12345','https://saqara.magdsoft.com/uploads/agents/9gcUkzKPdmOkMg2t2EJ9BiOaQKxt5a.jpg',1,'2018-08-11','silver','ali','ali'),(10,'محمد','023564789','12322014789',25,'hamoda@gmail.com',1,'12346','https://saqara.magdsoft.com/uploads/agents/vgCWQLu3vvEmQaSUZxAw2DfU5Bg6l1.jpg',1,'2018-08-11','gold','احمد','محمود'),(12,'يوسف','21159986587459','0544690006',90,'administrator@alsaqircentre.ae',1,'654654','https://saqara.magdsoft.com/uploads/agents/6WHCV0NCrIrAGUP5VdePSLf01FqU7W.png',0,'2018-08-19','gold','يوسف','محمد'),(13,'حسن','1254896464','010002556476',53,'abdelrhman_sayed22@yahoo.com',1,'6665','https://saqara.magdsoft.com/uploads/agents/vg39jojCklyksYbI7wCmrptpXoYI5j.jpeg',1,'2018-08-26','silver','عبد الغنى','شوقى');
/*!40000 ALTER TABLE `agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents_companies`
--

DROP TABLE IF EXISTS `agents_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents_companies` (
  `company_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  PRIMARY KEY (`company_id`,`agent_id`),
  KEY `FK_agent_company` (`agent_id`),
  CONSTRAINT `FK_agent_company` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_company_agent` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents_companies`
--

LOCK TABLES `agents_companies` WRITE;
/*!40000 ALTER TABLE `agents_companies` DISABLE KEYS */;
INSERT INTO `agents_companies` VALUES (3,4),(3,7),(3,10),(3,12),(3,13),(4,4),(4,7),(4,8),(4,10),(4,12),(5,9),(5,10),(5,12),(6,12);
/*!40000 ALTER TABLE `agents_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents_messages`
--

DROP TABLE IF EXISTS `agents_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `message_type_id` int(11) DEFAULT NULL,
  `agent_id` int(11) NOT NULL,
  `content` varchar(200) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_message_agent` (`message_type_id`),
  KEY `FK_agent_message` (`agent_id`),
  KEY `FK_admin_message` (`admin_id`),
  CONSTRAINT `FK_admin_message` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_agent_message` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_message_agent` FOREIGN KEY (`message_type_id`) REFERENCES `messages_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents_messages`
--

LOCK TABLES `agents_messages` WRITE;
/*!40000 ALTER TABLE `agents_messages` DISABLE KEYS */;
INSERT INTO `agents_messages` VALUES (21,'0','2018-07-07 00:00:00',1,7,'hiiiiiiiiiiiiiiiiiii',7),(22,'0','2018-07-07 00:00:00',1,1,'اشكرك علي تقديرك',5),(23,'0','2018-07-10 00:00:00',1,1,'dasdasdasdasdas',1),(24,'0','2018-07-10 00:00:00',1,1,'vdfsdfsd sdfsdfdfsd fsdfsd f',1),(25,'0','2018-07-10 00:00:00',1,1,'السلام عليكم اخي لديكم مشكلة في السيستم',1),(26,'0','2018-07-10 00:00:00',1,1,'pppppppppppppppppppppp',1),(27,'0','2018-07-10 00:00:00',2,4,'hi amr',1),(29,'0','2018-07-25 00:00:00',1,4,'عزيزنا العميل عماد شوقي محمد, تم تلقي شكواك بخصوص عدم عمل الخدمة الذاتية بشكل جيد, ونود إعلامك بأنه تم حل الشكوي المسجلة برقم 10536. نشكرك علي وقتك ونتمني قضاء يوم سعيد. تقنية المعلومات',1),(31,'0','2018-07-29 00:00:00',1,4,'اختبار جديد',7),(32,'0','2018-07-29 00:00:00',1,1,'اختبار محتوى ارسال',7),(34,'0','2018-07-30 00:00:00',2,1,'جديدددددددددددددددددددددددببببب',7);
/*!40000 ALTER TABLE `agents_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents_payments`
--

DROP TABLE IF EXISTS `agents_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` date NOT NULL,
  `total` double NOT NULL,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `is_late` tinyint(1) NOT NULL DEFAULT '0',
  `agent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agent_id` (`agent_id`),
  CONSTRAINT `agents_payments_ibfk_1` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents_payments`
--

LOCK TABLES `agents_payments` WRITE;
/*!40000 ALTER TABLE `agents_payments` DISABLE KEYS */;
INSERT INTO `agents_payments` VALUES (1,'2018-08-01',1700,1,0,4),(2,'2018-08-02',99.55,1,1,13);
/*!40000 ALTER TABLE `agents_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents_transactions`
--

DROP TABLE IF EXISTS `agents_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents_transactions` (
  `transaction_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`,`agent_id`),
  KEY `FK_agent_transaction` (`agent_id`),
  CONSTRAINT `FK_agent_transaction` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_transaction_agent` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents_transactions`
--

LOCK TABLES `agents_transactions` WRITE;
/*!40000 ALTER TABLE `agents_transactions` DISABLE KEYS */;
INSERT INTO `agents_transactions` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `agents_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_settings`
--

DROP TABLE IF EXISTS `app_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_settings` (
  `id` int(11) NOT NULL,
  `comission_per_transaction` int(11) NOT NULL,
  `payment_alerting_day` int(2) NOT NULL,
  `allowed_payment_period` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_settings`
--

LOCK TABLES `app_settings` WRITE;
/*!40000 ALTER TABLE `app_settings` DISABLE KEYS */;
INSERT INTO `app_settings` VALUES (1,5,27,1);
/*!40000 ALTER TABLE `app_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_en` varchar(60) NOT NULL,
  `name_ar` varchar(60) NOT NULL,
  `number` varchar(20) NOT NULL,
  `card_number` varchar(11) DEFAULT NULL,
  `license` varchar(11) DEFAULT NULL,
  `card_path` varchar(200) DEFAULT NULL,
  `company_path` varchar(200) DEFAULT NULL,
  `company_number` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (3,'magdsoft','مجد سوفت','46','7897ذ','897897','مصر','مصر','012345464'),(4,'google','جوجل','20','5645','54654','654654','546546','546546456'),(5,'start','ستارت','123456','123456','654321','654321','654321','01007676767'),(6,'tinker bill','تنة ورنة','12','123456789','0123654789','147','453','12345678'),(7,'etisalat co','شركة اتصالات','65465465',NULL,'654654654',NULL,NULL,'02656656');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complains`
--

DROP TABLE IF EXISTS `complains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  `suggestion` text COLLATE utf8mb4_unicode_ci,
  `created_at` date NOT NULL,
  `agent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agent_id` (`agent_id`),
  CONSTRAINT `complains_ibfk_1` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complains`
--

LOCK TABLES `complains` WRITE;
/*!40000 ALTER TABLE `complains` DISABLE KEYS */;
INSERT INTO `complains` VALUES (1,'this is a test message \r\n',4.5,'this is the suggestion for this message','2018-08-31',4);
/*!40000 ALTER TABLE `complains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields_translation`
--

DROP TABLE IF EXISTS `fields_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields_translation` (
  `field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `content` varchar(50) NOT NULL,
  PRIMARY KEY (`field_id`,`language_id`),
  KEY `FK_field_language` (`language_id`),
  CONSTRAINT `FK_field_language` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_field_translate` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields_translation`
--

LOCK TABLES `fields_translation` WRITE;
/*!40000 ALTER TABLE `fields_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `fields_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (3,'فرنسي'),(4,'الماني');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_history`
--

DROP TABLE IF EXISTS `login_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(100) NOT NULL,
  `device` varchar(200) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_admin_login` (`admin_id`),
  CONSTRAINT `FK_admin_login` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_history`
--

LOCK TABLES `login_history` WRITE;
/*!40000 ALTER TABLE `login_history` DISABLE KEYS */;
INSERT INTO `login_history` VALUES (2,'154.189.62.113','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-09 10:59:32'),(3,'154.189.62.113','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 11:01:52'),(4,'154.189.62.113','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-09 11:09:44'),(5,'154.189.62.113','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-09 11:09:54'),(6,'154.189.62.113','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 11:17:07'),(7,'154.189.62.113','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 11:18:53'),(8,'154.189.62.113','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-09 11:32:07'),(9,'154.189.62.113','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 11:44:58'),(10,'154.189.62.113','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 12:09:17'),(11,'154.189.62.113','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 12:10:25'),(12,'154.189.62.113','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 12:14:33'),(13,'105.206.90.139','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 03:46:55'),(14,'154.188.10.19','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-09 06:33:38'),(15,'197.42.214.136','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36 OPR/53.0.2907.99',1,'2018-07-10 08:17:15'),(16,'154.184.181.217','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',1,'2018-07-10 11:22:42'),(17,'154.184.181.217','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-10 12:51:12'),(18,'197.42.214.136','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-10 01:09:53'),(19,'197.42.214.136','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-10 01:10:08'),(20,'197.42.214.136','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-10 01:14:46'),(21,'92.98.150.193','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-10 03:08:55'),(22,'154.184.181.217','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-10 04:31:11'),(23,'41.69.226.246','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-11 07:54:05'),(24,'197.133.222.103','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-14 09:59:18'),(25,'92.98.150.193','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-15 04:27:02'),(26,'197.49.27.215','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-16 08:53:29'),(27,'154.184.17.138','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-16 01:32:58'),(28,'86.99.108.27','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-16 01:37:12'),(29,'86.99.108.27','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-16 03:45:17'),(30,'197.48.71.66','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-17 03:21:15'),(31,'86.99.108.27','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-17 05:03:02'),(32,'86.99.108.27','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-20 03:18:03'),(33,'154.189.121.7','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/67.0.3396.99 Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-23 05:38:58'),(34,'154.189.121.7','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/67.0.3396.99 Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-23 05:50:39'),(35,'197.48.138.118','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36 OPR/54.0.2952.54',1,'2018-07-24 10:13:50'),(36,'154.185.155.21','Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/67.0.3396.99 Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-24 05:07:59'),(37,'41.233.135.211','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/67.0.3396.99 Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-25 02:04:40'),(38,'41.233.135.211','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/67.0.3396.99 Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-25 02:04:48'),(39,'41.233.135.211','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/67.0.3396.99 Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-25 03:45:30'),(40,'41.233.135.211','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/67.0.3396.99 Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-25 05:14:58'),(41,'154.188.102.47','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-29 10:37:35'),(42,'154.188.102.47','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-29 02:45:07'),(43,'41.232.231.170','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-29 06:39:32'),(44,'41.232.231.170','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-29 06:39:42'),(45,'41.232.231.170','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-29 06:39:57'),(46,'41.232.231.170','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-29 06:40:08'),(47,'41.232.231.170','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-29 06:40:11'),(48,'154.184.69.13','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-30 08:56:45'),(49,'154.184.69.13','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-30 08:56:58'),(50,'154.184.69.13','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-30 08:57:54'),(51,'154.184.69.13','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-30 08:58:11'),(52,'41.232.144.54','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36 OPR/54.0.2952.64',1,'2018-07-30 03:06:25'),(53,'197.38.72.103','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/67.0.3396.99 Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-30 05:04:02'),(54,'154.189.50.119','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-31 09:23:49'),(55,'154.189.50.119','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-31 09:23:56'),(56,'154.189.50.119','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-31 09:24:32'),(57,'154.188.46.69','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-31 04:58:49'),(58,'154.188.46.69','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-07-31 04:59:07'),(59,'154.189.150.209','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-01 12:10:29'),(60,'154.185.94.153','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-04 11:13:07'),(61,'154.185.94.153','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-04 11:13:15'),(62,'154.185.94.153','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-04 11:14:53'),(63,'154.185.94.153','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/68.0.3440.75 Chrome/68.0.3440.75 Safari/537.36',1,'2018-08-04 12:56:51'),(64,'154.185.94.153','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-04 03:43:27'),(65,'154.185.94.153','Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0',1,'2018-08-04 03:44:24'),(66,'41.232.249.101','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 10:25:35'),(67,'41.232.249.101','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 10:25:52'),(68,'41.232.249.101','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 10:27:30'),(69,'41.232.249.101','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 10:27:54'),(70,'41.232.249.101','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 10:29:03'),(71,'41.232.249.101','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 10:29:23'),(72,'197.46.103.47','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 11:47:44'),(73,'197.46.103.47','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 11:49:21'),(74,'197.46.103.47','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 11:50:13'),(75,'197.46.103.47','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 11:57:13'),(76,'197.46.103.47','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-05 12:00:58'),(77,'154.188.168.254','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-05 01:20:50'),(78,'154.185.0.18','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-05 03:31:09'),(79,'154.185.189.40','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-06 11:22:55'),(80,'197.46.103.47','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36',1,'2018-08-06 03:07:39'),(81,'154.189.193.2','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-06 09:06:25'),(82,'86.99.108.27','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36',1,'2018-08-07 02:14:32'),(83,'41.238.61.70','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',1,'2018-08-08 12:48:22'),(84,'41.232.122.35','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36',1,'2018-08-08 02:44:12'),(85,'41.232.32.149','Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',1,'2018-08-11 10:47:08'),(86,'41.232.32.149','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36',1,'2018-08-11 10:59:03'),(87,'154.189.171.35','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-11 12:14:02'),(88,'154.189.171.35','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-11 12:14:13'),(89,'41.232.32.149','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36',1,'2018-08-12 09:56:02'),(90,'41.232.32.149','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-12 09:56:17'),(91,'41.232.32.149','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-12 01:11:59'),(92,'83.110.7.93','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-13 03:45:11'),(93,'83.110.7.93','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-13 03:45:24'),(94,'83.110.7.93','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-13 07:29:29'),(95,'154.188.154.102','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-13 09:20:46'),(96,'154.188.154.102','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-13 09:21:00'),(97,'41.232.32.149','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-13 10:19:34'),(98,'41.232.32.149','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-13 12:52:11'),(99,'86.99.108.27','Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.91 Mobile Safari/537.36',1,'2018-08-13 02:04:42'),(100,'154.184.95.31','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-18 12:41:55'),(101,'154.184.95.31','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-18 12:42:05'),(102,'154.184.95.31','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-18 12:42:19'),(103,'86.99.108.27','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-18 02:08:33'),(104,'83.110.7.93','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-19 04:53:45'),(105,'41.232.148.0','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-19 09:24:01'),(106,'86.99.108.27','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-20 08:54:39'),(107,'41.232.236.127','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 10:21:47'),(108,'41.232.236.127','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 11:49:42'),(109,'41.232.236.127','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 11:49:49'),(110,'41.232.236.127','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 11:50:02'),(111,'41.232.236.127','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 11:50:07'),(112,'41.232.236.127','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 11:50:11'),(113,'41.232.236.127','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 11:50:23'),(114,'41.232.236.127','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 11:50:44'),(115,'41.232.236.127','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 12:39:46'),(116,'41.232.236.127','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 12:43:09'),(117,'156.204.144.11','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-26 07:42:04'),(118,'86.99.108.27','Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.91 Mobile Safari/537.36',1,'2018-08-27 12:08:49'),(119,'92.99.122.193','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-27 02:54:16'),(120,'154.189.117.174','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-27 04:25:51'),(121,'154.189.117.174','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-27 04:27:40'),(122,'154.189.117.174','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-27 04:27:48'),(123,'154.189.117.174','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-27 04:28:03'),(124,'154.189.117.174','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-28 10:27:11'),(125,'154.189.117.174','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-28 02:15:49'),(126,'154.189.117.174','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-28 02:16:12'),(127,'154.189.117.174','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',7,'2018-08-28 02:47:54'),(128,'92.99.122.193','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-28 06:34:53'),(129,'154.188.202.204','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',7,'2018-08-29 01:29:17'),(130,'197.39.232.148','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',7,'2018-08-31 12:45:58'),(131,'197.39.232.148','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',7,'2018-08-31 01:22:49'),(132,'92.99.122.193','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-08-31 04:54:14'),(133,'197.46.93.168','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',7,'2018-09-03 09:58:16'),(134,'154.189.226.32','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',7,'2018-09-03 11:30:13'),(135,'154.189.61.198','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',7,'2018-09-03 01:40:08'),(136,'156.205.72.88','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-09-05 02:44:27'),(137,'154.189.102.141','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',7,'2018-09-05 03:00:43'),(138,'83.110.136.57','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-09-06 03:18:37'),(139,'197.46.103.79','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-09-08 10:40:09'),(140,'163.172.227.165','Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.91 Mobile Safari/537.36',1,'2018-09-08 01:01:07'),(141,'31.215.210.182','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-09-08 01:14:25'),(142,'31.215.210.182','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-09-09 04:24:27'),(143,'31.215.210.182','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',10,'2018-09-09 04:40:55'),(144,'31.215.210.182','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-09-09 04:58:24'),(145,'31.215.210.182','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',10,'2018-09-09 05:02:57'),(146,'31.215.210.182','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',1,'2018-09-09 05:14:48');
/*!40000 ALTER TABLE `login_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_type_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `content` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id` (`id`),
  KEY `FK_message_language` (`language_id`) USING BTREE,
  KEY `message_type_id` (`message_type_id`) USING BTREE,
  CONSTRAINT `FK_message_language` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_message_type` FOREIGN KEY (`message_type_id`) REFERENCES `messages_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,1,4,'gggggfffffffffffdf'),(2,5,4,'يجب الحضور لإستلام مستحقاتك لشهر 9');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages_types`
--

DROP TABLE IF EXISTS `messages_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages_types`
--

LOCK TABLES `messages_types` WRITE;
/*!40000 ALTER TABLE `messages_types` DISABLE KEYS */;
INSERT INTO `messages_types` VALUES (1,'تعديل خصم'),(2,'زيادات'),(5,'تنبيه');
/*!40000 ALTER TABLE `messages_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nationalites`
--

DROP TABLE IF EXISTS `nationalites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nationalites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nationalites`
--

LOCK TABLES `nationalites` WRITE;
/*!40000 ALTER TABLE `nationalites` DISABLE KEYS */;
INSERT INTO `nationalites` VALUES (1,'مصري'),(2,'تونسي'),(4,'هندى'),(5,'الماني'),(6,'سوري');
/*!40000 ALTER TABLE `nationalites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nationalites_translation`
--

DROP TABLE IF EXISTS `nationalites_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nationalites_translation` (
  `nationality_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `content` varchar(50) NOT NULL,
  PRIMARY KEY (`nationality_id`,`language_id`),
  KEY `FK_nationality_language` (`language_id`),
  CONSTRAINT `FK_nationality_language` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_nationality_translate` FOREIGN KEY (`nationality_id`) REFERENCES `nationalites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nationalites_translation`
--

LOCK TABLES `nationalites_translation` WRITE;
/*!40000 ALTER TABLE `nationalites_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `nationalites_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cost` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `status` enum('paid','unpaid') DEFAULT NULL,
  `is_late` tinyint(1) NOT NULL DEFAULT '0',
  `agent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_agent_payment` (`agent_id`),
  CONSTRAINT `FK_agent_payment` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,20,'2018-08-25','paid',0,13);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` enum('مدير النظام','مستخدم عادي','مستخدم التقارير') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'مدير النظام'),(2,'مستخدم عادي'),(3,'مستخدم التقارير');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `agent_id` int(11) NOT NULL,
  `is_successful` tinyint(1) NOT NULL DEFAULT '0',
  `cost` double(3,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agent_id` (`agent_id`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,'444444','2018-07-02 17:48:07',1,0,0.00),(2,'000000','2018-07-02 17:48:07',1,0,0.00),(3,'555555','2018-07-29 17:19:45',1,0,0.00),(4,'not numbers','2018-07-31 00:00:00',1,1,0.00),(5,'90909090','2018-07-31 11:41:12',4,1,0.00),(6,'12','2018-08-11 12:42:10',4,1,0.00),(7,'500','2018-08-26 11:57:33',1,1,0.00),(8,'1000','2018-08-26 11:58:52',1,1,0.00),(9,'2355','2018-08-26 12:02:04',4,1,0.00),(10,'1000','2018-08-26 12:05:12',13,1,0.00),(11,'1000','2018-08-26 12:06:47',13,1,0.00),(12,'500','2018-08-26 12:07:47',13,1,0.00),(13,'1000','2018-08-26 12:09:16',13,0,0.00),(14,'500','2018-08-26 12:13:55',13,1,0.00),(15,'11','2018-08-28 12:57:21',1,0,5.00),(16,'33','2018-08-28 12:57:35',4,0,5.00);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_blocking`
--

DROP TABLE IF EXISTS `user_blocking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_blocking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(200) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_blocking`
--

LOCK TABLES `user_blocking` WRITE;
/*!40000 ALTER TABLE `user_blocking` DISABLE KEYS */;
INSERT INTO `user_blocking` VALUES (3,'197.5.5.5',1);
/*!40000 ALTER TABLE `user_blocking` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-09  8:38:42
